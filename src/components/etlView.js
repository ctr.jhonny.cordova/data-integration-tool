import { useState, useRef, useEffect } from 'react';
// import { MyFileBrowser } from './fileBrowser';
import dynamic from 'next/dynamic';

const FileBrowser = dynamic(() => import('./fileBrowser'), {
  ssr: false,
})

export default function EtlView({ etl }) {
  const [file, setFile] = useState([]);

  const inputFile = useRef(null);

  const handleRunRowFileChange = e => {
    setFile([...file, e.target.files[0]]);
  }

  const handleRunNowClick = () => {
    if (etl.status === 'REHEARSAL') {
      inputFile.current.click();
    }
  }

  return (
    <div>
      <h2 className="mb-4">{etl.name}</h2>
      <div className="card bg-gray-300 shadow-none mb-4">
        <div className="card-body">
          <div className="d-flex justify-content-between
                          align-items-center">
            <div>
              <h6 className="mb-3">{etl.scheduled}</h6>

              {/* <h6 className="mb-3"><strong>{etl.status}</strong></h6> */}

              {etl.currentVersion && (
                <div>
                  <h6 className="mb-3"><strong>{etl.currentVersion}</strong></h6>
                </div>
              )}

              {etl.activeVersion && (
                <div>
                  <h6 className="mb-3"><strong>{etl.activeVersion}</strong></h6>
                </div>
              )}
            </div>
          </div>

          <div>
            <button type="button" class="btn btn-success me-2">Clone this version</button>
            {etl.status === 'REHEARSAL' && (
              <button type="button" class="btn btn-primary">Publish</button>
            )}
            {etl.status === 'ACTIVE' && (
              <button type="button" class="btn btn-danger">Disable</button>
            )}
            {etl.id === 2 && (
              <button type="button" class="btn btn-secondary ms-2">Diff versions</button>
            )}
          </div>
        </div>
      </div>
      <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
        {/* <li className="nav-item" role="presentation">
          <button className="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Status</button>
        </li> */}
        <li className="nav-item" role="presentation">
          <button className="nav-link active" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Job History</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Source</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-transformation-tab" data-bs-toggle="pill" data-bs-target="#pills-transformation" type="button" role="tab" aria-controls="pills-transformation" aria-selected="false">Transformation</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-destination-tab" data-bs-toggle="pill" data-bs-target="#pills-destination" type="button" role="tab" aria-controls="pills-destination" aria-selected="false">Destination</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-versions-tab" data-bs-toggle="pill" data-bs-target="#pills-versions" type="button" role="tab" aria-controls="pills-versions" aria-selected="false">Versions</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-change-histories-tab" data-bs-toggle="pill" data-bs-target="#pills-change-histories" type="button" role="tab" aria-controls="pills-change-histories" aria-selected="false">Change History</button>
        </li>
      </ul>
      <div className="tab-content" id="pills-tabContent">
        <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
          {/* <div className="card bg-gray-300 shadow-none mb-4">
            <div className="card-body">
              <div className="d-flex justify-content-between
                          align-items-center">
                <div>
                  <h6 className="mb-3">{etl.scheduled}</h6>

                  <h6 className="mb-3"><strong>{etl.status}</strong></h6>

                  {etl.activeVersion && (
                    <div>
                      <h6 className="mb-3">ACTIVE VERSION: {etl.activeVersion}</h6>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div> */}
        </div>
        <div className="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" tabindex="0">
          <div className="mb-8">

            <div className='mb-3'>

              <div className="">
                <div className="d-flex justify-content-between
                            align-items-center">
                  <div>
                    <h6 className="mb-0">&nbsp;</h6>
                  </div>
                  <div>
                    {etl.status === 'REHEARSAL' && (
                      <>
                        <button type="button" className="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal">
                          Run now
                        </button>
                        
                        <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div className="modal-dialog modal-xl">
                            <div className="modal-content">
                              <div className="modal-header">
                                <h1 className="modal-title fs-5" id="exampleModalLabel">Files in Bucket</h1>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>
                              <div className="modal-body">
                                <FileBrowser />
                              </div>
                              <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </>
                    )}

                    {etl.status !== 'REHEARSAL' && (
                      <button type="button" class="btn btn-success btn-sm" onClick={() => {}}>Run now</button>
                    )}

                  </div>
                </div>
              </div>
            </div>

            

            {etl.jobHistory.map((job, index) => (
              <div className="card" key={index}>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item p-3">
                    <div className="d-flex justify-content-between
                              align-items-center">
                      <div className="d-flex align-items-center">

                        <div>
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="green" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
                            <path d="M20 6L9 17l-5-5"></path>
                          </svg>
                        </div>

                        <div className="ms-3">
                          <p className="mb-0
                                    font-weight-medium">

                            {/* <Link href={'/etl/wip'} className="link-dark rounded"> */}
                            <a href='#' className="link-dark link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover" data-bs-toggle="modal" data-bs-target={`#exampleModal${index}`}>
                              [{job.date}] {job.title}
                            </a>
                            {/* </Link> */}
                          </p>
                          <p className="mb-0 font-weight-medium fw-lighter">{job.detail}</p>
                        </div>
                      </div>
                      <div>

                        <div className="dropdown dropstart">
                          <a href="#!" className="btn btn-ghost btn-icon btn-sm rounded-circle" id="dropdownactivityOne" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical icon-xs"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                          </a>
                          {/* <div className="dropdown-menu" aria-labelledby="dropdownactivityOne">
                            <a className="dropdown-item d-flex align-items-center" href="#!">Action</a>
                            <a className="dropdown-item d-flex align-items-center" href="#!">Another action</a>
                            <a className="dropdown-item d-flex align-items-center" href="#!">Something else
                              here</a>
                          </div> */}
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>

                <div class="modal fade" id={`exampleModal${index}`} tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Job Logs</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        <div className="mb-3">
                          <label className="form-label">
                            <strong>ID</strong>
                          </label>
                          <div>{job.jobId}</div>
                        </div>
                        <div className="mb-3">
                          <label className="form-label">
                            <strong>Status</strong>
                          </label>
                          <div>{job.title}</div>
                        </div>
                        <div className="mb-3">
                          <label className="form-label">
                            <strong>Summary</strong>
                          </label>
                          <div>{job.detail}</div>
                        </div>
                        {job.results && (
                          <>
                            <h6 className="mb-3">Logs</h6>
                            <table className="table">
                              {/* <thead>
                                <tr>
                                  <th scope="col">&nbsp;</th>
                                </tr>
                              </thead> */}
                              <tbody>
                                {job.results.map((result, index) => (
                                  <tr key={index}>
                                    {/* <td>{result.stage}</td> */}
                                    <td>{result.stage}: {result.detail}</td>
                                  </tr>
                                ))}
                              </tbody>
                            </table>
                          </>
                        )}

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab" tabindex="0">
          <div className="card bg-gray-300 shadow-none mb-4">
            <div className="card-body">
              <div className="d-flex justify-content-between
                          align-items-center">
                <div className="container">

                  {etl.source.type === 'FILE_S3' && (
                    <FileS3Source source={etl.source} />
                  )}

                  {etl.source.type === 'MySQL' && (
                    <MySQLSource source={etl.source} />
                  )}

                  {etl.source.type === 'FILE_HDFS' && (
                    <HDFSSource source={etl.source} />
                  )}


                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tab-pane fade show" id="pills-transformation" role="tabpanel" aria-labelledby="pills-transformation-tab" tabindex="0">
          <div className="card bg-gray-300 shadow-none mb-4">
            <div className="card-body">
              <div className="d-flex justify-content-between
                          align-items-center">
                <div className="container">

                  <div className="row">
                    <div className="col-8">
                      <fieldset className="form-group border p-3 mb-3">
                        <legend>Trigger Information</legend>

                        <div className="mb-3">
                          <label className="form-label">
                            <strong>Type</strong>
                          </label>
                          <div>{etl.trigger.type}</div>
                        </div>
                      </fieldset>

                      <legend>Stages details</legend>

                      <div className="accordion" id="accordionExample">

                        {etl.stages.map((stage, index) => (
                          <StageDetail key={index} stage={stage} />
                        ))}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tab-pane fade show" id="pills-destination" role="tabpanel" aria-labelledby="pills-destination-tab" tabindex="0">
          <div className="card bg-gray-300 shadow-none mb-4">
            <div className="card-body">
              <div className="d-flex justify-content-between
                          align-items-center">
                <div className="container">

                  <div className="row">
                    <div className="accordion" id="accordionExample">
                      {etl.destinations.map((destination, index) => (
                        <div className="accordion-item" key={index}>
                          <h2 className="accordion-header">
                            <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target={'#collapse' + index} aria-expanded="true" aria-controls={'collapse' + index}>
                              Destination for Stage #{destination.stage_index} {destination.stage_name}
                            </button>
                          </h2>
                          <div id={'collapse' + +index} className="accordion-collapse collapse show">
                            <div className="accordion-body">
                              <div className="row">
                                <div className="col-6">
                                  <h3 className="mb-3">{destination.name}</h3>
                                  <h6 className="mb-3">Destination details</h6>

                                  {destination.type === 'MYSQL' && (
                                    <div className="mb-3">
                                      <label className="form-label">
                                        <strong>Table Name</strong>
                                      </label>
                                      <div>{destination.tableName}</div>
                                    </div>
                                  )}

                                  {destination.type === 'KAFKA' && (
                                    <div className="mb-3">
                                      <label className="form-label">
                                        <strong>Topic</strong>
                                      </label>
                                      <div>{destination.topic}</div>
                                    </div>
                                  )}

                                </div>
                                {/* <div className="col-6">
                                  <h3 className="mb-3">&nbsp;</h3>
                                  <h4 className="mb-3">Testing</h4>
                                  <h6 className="mb-3">Destination details</h6>
                                  <div className="mb-3">
                                    <label className="form-label">
                                      <strong>Host</strong>
                                    </label>
                                    <div>{destination.testing.host}</div>
                                  </div>
                                  <div className="mb-3">
                                    <label className="form-label">
                                      <strong>Port</strong>
                                    </label>
                                    <div>{destination.testing.port}</div>
                                  </div>
                                  <div className="mb-3">
                                    <label className="form-label">
                                      <strong>Database</strong>
                                    </label>
                                    <div>{destination.testing.database}</div>
                                  </div>
                                  <div className="mb-3">
                                    <label className="form-label">
                                      <strong>Username</strong>
                                    </label>
                                    <div>{destination.testing.username}</div>
                                  </div>
                                  <div className="mb-3">
                                    <label className="form-label">
                                      <strong>Password</strong>
                                    </label>
                                    <div>******</div>
                                  </div>
                                  <h6 className="mb-3">Specify details for this connection</h6>
                                  <div className="mb-3">
                                    <label className="form-label">
                                      <strong>Table Name</strong>
                                    </label>
                                    <div>{destination.testing.tableName}</div>
                                  </div>
                                </div> */}
                              </div>

                            </div>
                          </div>
                        </div>
                      ))}


                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tab-pane fade" id="pills-versions" role="tabpanel" aria-labelledby="pills-versions-tab" tabindex="0">
          <div className="mb-8">

            <div className="card bg-gray-300 shadow-none mb-4">

              <div className="card-body">
                <div className="d-flex justify-content-between
                            align-items-center">
                  <div>
                    <h6 className="mb-0">Versions</h6>
                  </div>
                </div>
              </div>
            </div>

            <div className="card">

              <ul className="list-group list-group-flush">

                {etl.versions.map((version, index) => (
                  <li className="list-group-item p-3" key={index}>
                    <div className="d-flex justify-content-between
                              align-items-center">
                      <div className="d-flex align-items-center">

                        {version.active && (
                          <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="green" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
                              <path d="M20 6L9 17l-5-5"></path>
                            </svg>
                          </div>
                        )}

                        <div className="ms-3">
                          <p className="mb-0
                                      font-weight-medium">[Created at {version.date}] Version {version.version}</p>
                          <p className="mb-0 font-weight-medium fw-lighter">{version.status} {version.description}</p>
                        </div>
                      </div>
                    </div>
                  </li>
                ))}


              </ul>
            </div>
          </div>
        </div>
        <div className="tab-pane fade" id="pills-change-histories" role="tabpanel" aria-labelledby="pills-change-histories-tab" tabindex="0">
          <div className="mb-8">

            {/* <div className="card bg-gray-300 shadow-none mb-4">

              <div className="card-body">
                <div className="d-flex justify-content-between
                            align-items-center">
                  <div>
                    <h6 className="mb-0"></h6>
                  </div>
                </div>
              </div>
            </div> */}

            <div className="card">

              <ul className="list-group list-group-flush">
                {etl.changes.map((change, index) => (
                  <li className="list-group-item p-3" key={index}>
                    <div className="d-flex justify-content-between
                              align-items-center">
                      <div className="d-flex align-items-center">
                        <div className="ms-3">
                          <p className="mb-0
                                      font-weight-medium"><strong>{change.user}</strong> {change.action} the ETL at {change.date}</p>
                          {/* <p className="mb-0 font-weight-medium fw-lighter">{version.status}</p> */}
                        </div>
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const StageDetail = ({ stage }) => {
  // const [viewTesting, setViewTesting] = useState(false);
  // const [showSpinner, setShowSpinner] = useState(false);

  // useEffect(() => {
  //   if (viewTesting) {
  //     setShowSpinner(true);
  //     const timer = setTimeout(() => {
  //       setShowSpinner(false);
  //     }, 2000);

  //     return () => clearTimeout(timer);

  //   }
  // }, [viewTesting]);

  // const showPreview = () => {
  //   setViewTesting(!viewTesting);
  // };

  return (
    <div className="accordion-item">
      <h2 className="accordion-header">
        <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target={'#collapse' + stage.index} aria-expanded="true" aria-controls={'collapse' + stage.index}>
          Stage #{stage.index} {stage.isOutput && `: ${stage.outputName}`}
        </button>
      </h2>
      <div id={'collapse' + stage.index} className="accordion-collapse collapse show">
        {/* <button type="button" className="btn btn-primary btn-sm float-end mt-3 me-3" onClick={showPreview}>Preview</button> */}
        <div className="accordion-body">
          <div className="mb-3">
            <label className="form-label">
              <strong>Type</strong>
            </label>
            <div>{stage.type}</div>
          </div>
          {stage.type === 'SQL' && (
            <div className="mb-3">
              <label className="form-label">
                <strong>SQL code</strong>
              </label>
              <div className="Code">
                <pre>
                  <code className={`language-sql`}>{stage.code}</code>
                </pre>
              </div>
            </div>
          )}

          {stage.type === 'PARSER' && (
            <>
              <div className="mb-3">
                <label className="form-label">
                  <strong>File Format</strong>
                </label>
                <div>{stage.fileFormat}</div>
              </div>
              <div className="mb-3">
                <label className="form-label">
                  <strong>Field Delimiter</strong>
                </label>
                <div>{stage.fieldDelimiter}</div>
              </div>
              <div className="mb-3">
                <label className="form-label">
                  <strong>Record Delimiter</strong>
                </label>
                <div><code>{stage.recordDelimiter}</code></div>
              </div>
              <div className="mb-3">
                <label className="form-label">
                  <strong>View Name</strong>
                </label>
                <div><code>{stage.viewName}</code></div>
              </div>
            </>

          )}

          {stage.type === 'REGEX_FILE' && (
            <>
              <div className="mb-3">
                <label className="form-label">
                  <strong>Regex</strong>
                </label>
                <div>{stage.Regex}</div>
              </div>
            </>
          )}

          {stage.type === 'SIMPLE' && (
            <>
              <div className="mb-3">
                <label className="form-label">
                  <strong>Code</strong>
                </label>
                <div className="Code">
                  <pre>
                    <code className={`language-sql`}>{stage.code}</code>
                  </pre>
                </div>
              </div>
            </>

          )}

          <div className="mb-3">
            <label className="form-label">
              <strong>Is Output</strong>
            </label>
            <div>{stage.isOutput ? 'Yes' : 'No'}</div>
          </div>
          {stage.isOutput && (
            <div className="mb-3">
              <label className="form-label">
                <strong>Output Name</strong>
              </label>
              <div>{stage.outputName}</div>
            </div>
          )}

          {/* <div className="mb-3">

            {viewTesting && showSpinner && (
              <div class="d-flex justify-content-center">
                <div class="spinner-border" role="status">
                  <span class="visually-hidden">Loading...</span>
                </div>
              </div>
            )}

            {viewTesting && !showSpinner && (
              <div>
                <PreviewExample stage={stage} />
              </div>
            )}
          </div> */}
        </div>
      </div>
    </div>
  )
}

const FileS3Source = ({ source }) => {
  return (
    <div className="row">
      <div className="col-6">

        <h3 className="mb-3">{source.name}</h3>
        {/* <h5 className="mb-3">Production</h5> */}
        <h6 className="mb-3">Source Details</h6>

        <div className="mb-3">
          <label className="form-label">
            <strong>Bucket</strong>
          </label>
          <div>{source.bucket}</div>
        </div>
        <div className="mb-3">
          <label className="form-label">
            <strong>File Pattern</strong>
          </label>
          <div>{source.filePattern}</div>
        </div>
      </div>
    </div>
  )
}

const HDFSSource = ({ source }) => {
  return (
    <div className="row">
      <div className="col-6">

        <h3 className="mb-3">{source.name}</h3>
        {/* <h5 className="mb-3">Production</h5> */}
        <h6 className="mb-3">Source Details</h6>

        <div className="mb-3">
          <label className="form-label">
            <strong>Connection URL</strong>
          </label>
          <div>{source.connectionUrl}</div>
        </div>
      </div>
    </div>
  )
}


const MySQLSource = ({ source }) => {
  return (
    <div className="row">
      <div className="col-6">

        <h3 className="mb-3">{source.name}</h3>
        <h5 className="mb-3">Production</h5>
        <h6 className="mb-3">Source Details</h6>

        <div className="mb-3">
          <label className="form-label">
            <strong>Host</strong>
          </label>
          <div>{source.production.host}</div>
        </div>
        <div className="mb-3">
          <label className="form-label">
            <strong>Port</strong>
          </label>
          <div>{source.production.port}</div>
        </div>
        <div className="mb-3">
          <label className="form-label">
            <strong>Database</strong>
          </label>
          <div>{source.production.database}</div>
        </div>
        <div className="mb-3">
          <label className="form-label">
            <strong>Username</strong>
          </label>
          <div>{source.production.username}</div>
        </div>
        <div className="mb-3">
          <label className="form-label">
            <strong>Password</strong>
          </label>
          <div>******</div>
        </div>
        <h6 className="mb-3">Specify details for this connection</h6>
        <div className="mb-3">
          <label className="form-label">
            <strong>Table Name</strong>
          </label>
          <div>{source.production.tableName}</div>
        </div>
      </div>
      <div className="col-6">
        <h3 className="mb-3">&nbsp;</h3>
        <h3 className="mb-3">Testing</h3>
        <h6 className="mb-3">Source Details</h6>
        <div className="mb-3">
          <label className="form-label">
            <strong>Host</strong>
          </label>
          <div>{source.testing.host}</div>
        </div>
        <div className="mb-3">
          <label className="form-label">
            <strong>Port</strong>
          </label>
          <div>{source.testing.port}</div>
        </div>
        <div className="mb-3">
          <label className="form-label">
            <strong>Database</strong>
          </label>
          <div>{source.testing.database}</div>
        </div>
        <div className="mb-3">
          <label className="form-label">
            <strong>Username</strong>
          </label>
          <div>{source.testing.username}</div>
        </div>
        <div className="mb-3">
          <label className="form-label">
            <strong>Password</strong>
          </label>
          <div>******</div>
        </div>
        <h6 className="mb-3">Specify details for this connection</h6>
        <div className="mb-3">
          <label className="form-label">
            <strong>Table Name</strong>
          </label>
          <div>{source.testing.tableName}</div>
        </div>
      </div>
    </div>
  )
}