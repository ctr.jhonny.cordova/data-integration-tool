
import { ChonkyIconFA } from 'chonky-icon-fontawesome';
import {
    FileBrowser,
    FileList,
    ChonkyActions
} from 'chonky';

const MyFileBrowser = () => {
    const files = [
        {
            id: 'mcd',
            name: 'payments.csv',
        },
    ];
    const folderChain = [{ id: 'xcv', name: 'Demo bucket', isDir: true }];
    return (
        <div style={{ height: 300 }}>
            <FileBrowser files={files} folderChain={folderChain} iconComponent={ChonkyIconFA} defaultFileViewActionId={ChonkyActions.EnableListView.id} >
                <FileList  />
            </FileBrowser>
        </div>
    );
};

export default MyFileBrowser;