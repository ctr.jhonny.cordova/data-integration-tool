import Prism from "prismjs";
import "prismjs/components/prism-sql";
import "prismjs/themes/prism.css";
import { useEffect } from 'react';

import EtlView from "../../components/etlView";

export default function Page() {

  const code = `
  CREATE VIEW SalesSummary AS
  SELECT 
    Customers.CustomerName, 
    Products.ProductName, 
    SUM(OrderDetails.Quantity) AS TotalQuantity, 
    SUM(OrderDetails.Quantity * Products.Price) AS TotalSales
  FROM Customers
  INNER JOIN Orders ON Customers.CustomerID = Orders.CustomerID
  INNER JOIN OrderDetails ON Orders.OrderID = OrderDetails.OrderID
  INNER JOIN Products ON OrderDetails.ProductID = Products.ProductID
  GROUP BY Customers.CustomerName, Products.ProductName;
`;

  const code2 = `
CREATE VIEW FilteredSalesSummary AS
  SELECT 
    Customers.CustomerName, 
    Products.ProductName, 
    SUM(OrderDetails.Quantity) AS TotalQuantity, 
    SUM(OrderDetails.Quantity * Products.Price) AS TotalSales
  FROM Customers
  INNER JOIN Orders ON Customers.CustomerID = Orders.CustomerID
  INNER JOIN OrderDetails ON Orders.OrderID = OrderDetails.OrderID
  INNER JOIN Products ON OrderDetails.ProductID = Products.ProductID
  WHERE Customers.Country = 'USA' AND Orders.OrderDate >= '2022-01-01'
  GROUP BY Customers.CustomerName, Products.ProductName
  ORDER BY TotalSales DESC;
`;

  const etl = {
    name: 'MySQL Server to PRTH MySQL (Process 1)',
    status: 'DRAFT',
    scheduled: 'Trigger by a cronjob every 8 hours',
    trigger: {
      type: 'Manual'
    },
    stages: [
      {
        index: 1,
        type: 'SQL',
        code: code,
        isOutput: false,
      },
      {
        index: 2,
        type: 'SQL',
        code: code2,
        isOutput: true,
        outputName: 'sales_view'
      }
    ],
    jobHistory: [
      {
        title: 'Sync Succeeded',
        date: '10:00AM 02/27/2024',
        detail: '100 records extracted | 100 records loaded | JobId: 123 | 17s',
        jobId: 123,
        results: [
          {
            stage: 'Stage #1',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #: sales_view',
            detail: '100 records loaded',
          }
        ]
      },
      {
        title: 'Sync Succeeded',
        date: '11:00AM 02/28/2024',
        detail: '200 records extracted | 200 records loaded | JobId: 456 | 17s',
        jobId: 456,
        results: [
          {
            stage: 'Stage #1',
            detail: '200 records extracted',
          },
          {
            stage: 'Stage #: sales_view',
            detail: '200 records loaded',
          }
        ]
      },
      {
        title: 'Sync Failed',
        date: '11:00AM 02/29/2024',
        detail: '0 records extracted | 0 records loaded | JobId: 789 | 2m',
        jobId: 789,
        results: [
          {
            stage: 'Stage #1',
            detail: '0 records extracted',
          },
          {
            stage: 'Stage #: sales_view',
            detail: '0 records loaded',
          }
        ]
      },
      {
        title: 'Sync Succeeded',
        date: '11:00AM 02/28/2024',
        detail: '200 records extracted | 200 records loaded | JobId: 0001 | 17s',
        jobId: "0001",
        results: [
          {
            stage: 'Stage #1',
            detail: '200 records extracted',
          },
          {
            stage: 'Stage #: sales_view',
            detail: '200 records loaded',
          }
        ]
      }
    ],
    source: {
      name: 'MySQL Server',
      type: 'MySQL',
      production: {
        host: 'prth-rds-amazon.com',
        port: '1110',
        database: 'iam_capabillity',
        username: 'app_user',
        password: '******',
        tableName: 't_user'
      },
      testing: {
        host: 'prth-TEST-rds-amazon.com',
        port: '1110',
        database: 'iam_capabillity',
        username: 'app_user',
        password: '******',
        tableName: 't_user'
      }
    },
    destinations: [
      {
        stage_index: 2,
        stage_name: 'sales_view',
        name: 'PRTH MySQL',
        type: 'MySQL',
        tableName: 'prth_payments',
      }
    ],
    versions: [
      {
        version: '1.0',
        date: '02/27/2024',
        description: 'last modified by Jhonny Córdova at 02/27/2024 10:00AM ',
        status: 'DRAFT',
        active: true
      },
    ],
    changes: [
      {
        date: '02/27/2024 12:00:00',
        action: 'created',
        user: 'Jhonny Córdova',
      },
      {
        date: '02/29/2024 12:00:00',
        action: 'published',
        user: 'Jhonny Córdova',
      },
      {
        date: '03/03/2024 12:00:00',
        action: 'unpublished',
        user: 'Jhonny Córdova',
      },
    ]
  };

  useEffect(() => {
    Prism.highlightAll();
  }, []);

  return (
    <>
      <EtlView etl={etl} />
    </>
  );
}

