import Prism from "prismjs";
import "prismjs/components/prism-sql";
import "prismjs/themes/prism.css";
import { useEffect } from 'react';

import EtlView from "../../components/etlView";

export default function Page() {

  const code1 = `
CREATE OR REPLACE TEMPORARY VIEW df_files_mapped AS
SELECT
	id AS group_id01,
	c1 AS abaFRB,
	COALESCE(c2, '') AS receiverIdentification,
	to_date(c3, 'yyMMdd') AS fileCreationDate,
	c4 AS fileCreationTime,
	c5 AS fileIdentificationNumber,
	c6 AS physicalRecordLength,
	c7 AS blockSize,
	c8 AS versionNumber
FROM BankActivities_01
  `;

  const code2 = `
CREATE OR REPLACE TEMPORARY VIEW df_groups_mapped AS
SELECT
	id AS group_id02,
	group_id01,
	c3 AS groupStatus,
	to_date(c4, 'yyMMdd') AS asOfDate,
	c5 AS asOfTime,
	c7 AS asOfDateModifier
FROM BankActivities_02
`;
  const code3 = `
CREATE OR REPLACE TEMPORARY VIEW df_accounts_mapped AS
WITH combined AS (
SELECT
 line_number,
 id,
 group_id01,
 group_id02,
 COALESCE(c1, '') AS customerAccountNumber,
 c2 AS currencyCode,
 array(
   c3, c4, c5, c6, c7, c8, c9, c10, c11, c12,
   c13, c14, c15, c16, c17, c18, c19, c20, c21, c22,
   c23, c24, c25, c26, c27, c28, c29, c30, c31, c32,
   c33, c34, c35, c36, c37, c38, c39, c40, c41, c42,
   c43, c44, c45, c46, c47, c48, c49, c50, c51, c52,
   c53, c54
 ) AS array_col
FROM BankActivities_03
),
exploded AS (
SELECT
  c.*,
  pos,
  value,
  CASE
   WHEN value = '010' THEN pos
  END AS index010,
  CASE
   WHEN value = '015' THEN pos
  END AS index015,
  CASE
   WHEN value = '020' THEN pos
  END AS index020,
  CASE
   WHEN value = '025' THEN pos
  END AS index025,
  CASE
   WHEN value = '040' THEN pos
  END AS index040,
  CASE
   WHEN value = '045' THEN pos
  END AS index045,
  CASE
   WHEN value = '050' THEN pos
  END AS index050,
  CASE
   WHEN value = '055' THEN pos
  END AS index055,
  CASE
   WHEN value = '072' THEN pos
  END AS index072,
  CASE
   WHEN value = '074' THEN pos
  END AS index074,
  CASE
   WHEN value = '075' THEN pos
  END AS index075,
  CASE
   WHEN value = '100' THEN pos
  END AS index100,
  CASE
   WHEN value = '159' THEN pos
  END AS index159,
  CASE
   WHEN value = '400' THEN pos
  END AS index400,
  CASE
   WHEN value = '459' THEN pos
  END AS index459
FROM combined c
LATERAL VIEW posexplode(array_col) exploded_table AS pos, value
),
collapsed AS (
SELECT
   id,
   FIRST_VALUE(group_id01) AS group_id01,
   FIRST_VALUE(group_id02) AS group_id02,
   FIRST_VALUE(customerAccountNumber) AS customerAccountNumber,
   FIRST_VALUE(currencyCode) AS currencyCode,
   FIRST_VALUE(array_col) AS array_col,
   MAX(index010) AS index010,
   MAX(index015) AS index015,
   MAX(index020) AS index020,
   MAX(index025) AS index025,
   MAX(index040) AS index040,
   MAX(index045) AS index045,
   MAX(index050) AS index050,
   MAX(index055) AS index055,
   MAX(index072) AS index072,
   MAX(index074) AS index074,
   MAX(index075) AS index075,
   MAX(index100) AS index100,
   MAX(index159) AS index159,
   MAX(index400) AS index400,
   MAX(index459) AS index459
FROM exploded e
GROUP BY id
)
SELECT
 id AS group_id03,
 group_id01,
 group_id02,
 customerAccountNumber,
 currencyCode,
 array_col[index010 + 1] AS openingLedgerAmount,
 array_col[index010 + 2] AS openingLedgerCount,
 array_col[index010 + 3] AS openingLedgerFundsType,
 array_col[index015 + 1] AS closingLedgerAmount,
 array_col[index015 + 2] AS closingLedgerCount,
 array_col[index015 + 3] AS closingLedgerFundsType,
 array_col[index020 + 1] AS averageClosingLedgerMTDAmount,
 array_col[index020 + 2] AS averageClosingLedgerMTDCount,
 array_col[index020 + 3] AS averageClosingLedgerMTDFundsType,
 array_col[index025 + 1] AS averageClosingLedgerYTDAmount,
 array_col[index025 + 2] AS averageClosingLedgerYTDCount,
 array_col[index025 + 3] AS averageClosingLedgerYTDFundsType,
 array_col[index040 + 1] AS collectedPlus1DayAmount,
 array_col[index040 + 2] AS collectedPlus1DayCount,
 array_col[index040 + 3] AS collectedPlus1DayFundsType,
 array_col[index045 + 1] AS openingCollectedAmount,
 array_col[index045 + 2] AS openingCollectedCount,
 array_col[index045 + 3] AS openingCollectedFundsType,
 array_col[index050 + 1] AS averageClosingAvailableMTDAmount,
 array_col[index050 + 2] AS averageClosingAvailableMTDCount,
 array_col[index050 + 3] AS averageClosingAvailableMTDFundsType,
 array_col[index055 + 1] AS averageClosingAvailableYTDAmount,
 array_col[index055 + 2] AS averageClosingAvailableYTDCount,
 array_col[index055 + 3] AS averageClosingAvailableYTDFundsType,
 array_col[index072 + 1] AS oneDayFloatAmount,
 array_col[index072 + 2] AS oneDayFloatCount,
 array_col[index072 + 3] AS oneDayFloatFundsType,
 array_col[index074 + 1] AS twoDayFloatAmount,
 array_col[index074 + 2] AS twoDayFloatCount,
 array_col[index074 + 3] AS twoDayFloatFundsType,
 array_col[index075 + 1] AS threePlusDayFloatAmount,
 array_col[index075 + 2] AS threePlusDayFloatCount,
 array_col[index075 + 3] AS threePlusDayFloatFundsType,
 array_col[index100 + 1] AS totalCreditsAmount,
 array_col[index100 + 2] AS totalCreditsCount,
 array_col[index100 + 3] AS totalCreditsFundsType,
 array_col[index159 + 1] AS creditsRealTimePaymentAmount,
 array_col[index159 + 2] AS creditsRealTimePaymentCount,
 array_col[index159 + 3] AS creditsRealTimePaymentFundsType,
 array_col[index400 + 1] AS totalDebitsAmount,
 array_col[index400 + 2] AS totalDebitsCount,
 array_col[index400 + 3] AS totalDebitsFundsType,
 array_col[index459 + 1] AS debitsRealTimePaymentAmount,
 array_col[index459 + 2] AS debitsRealTimePaymentCount,
 array_col[index459 + 3] AS debitsRealTimePaymentFundsType
FROM collapsed c
`;
  const code4 = `
CREATE OR REPLACE TEMPORARY VIEW df_transactions_mapped AS
SELECT
 line_number,
 id AS group_id04,
 group_id01,
 group_id02,
 group_id03,
 c1 AS transactionTypeCode,
 CAST(c2 AS DOUBLE)/100.0 AS transactionAmount,
 c3 AS transactionFundsType,
 c4 AS bankReferenceNumber,
 c5 AS customerReferenceNumber,
 c6 AS transactionDetailType,
 CASE
   WHEN c6 LIKE 'ACH RETURN%' THEN 'ACH RETURN'
   WHEN c6 LIKE "AETRS SE PAYABLES NDING ACCTATS%" THEN 'AETRS SE PAYABLES ATS'
   WHEN c6 LIKE "AETRS SE PAYABLES NDING ACCTAMERICAN EXPRESS%" THEN 'AETRS SE PAYABLES AMERICAN EXPRESS'
   WHEN c6 LIKE "BANKCARD MERCH FEES%" THEN 'MERCH FEES'
   WHEN c6 LIKE "BANKCARD DEP BWH - %" THEN CONCAT('BWH - ', regexp_extract(c6, "^BANKCARD DEP BWH - (.*) (.*) (.*)$", 1))
   WHEN c6 LIKE "BANKCARD DEP DAILY DISC%" THEN 'DAILY DISC'
   WHEN c6 LIKE "BANKCARD DEP DAILY INT%" THEN 'DAILY INT'
   WHEN c6 LIKE "BANKCARD DEP MERCH ADJ%" THEN 'MERCH ADJ'
   WHEN c6 LIKE "BANKCARD DEP MERCH CHBK%" THEN 'MERCH CHBK'
   WHEN c6 LIKE "BANKCARD DEP MERCH DEP%" THEN 'MERCH DEP'
   WHEN c6 LIKE "DISCOVER FINANCIALERVICES LLC%CAP%" THEN 'DISCOVER FINANCIAL SERVICES LLC CAP'
   WHEN c6 LIKE "DISCOVER FINANCIALERVICES LLC%ATS%" THEN 'DISCOVER FINANCIAL SERVICES LLC ATS'
   WHEN c6 LIKE "DISCOVER NETWORK SETTLEMENT%" THEN 'DISCOVER NETWORK SETTLEMENT'
   WHEN c6 LIKE "MASTERCARD INTER MCBS DRAFT%" THEN 'MASTERCARD INTER MCBS DRAFT'
   WHEN c6 LIKE "MASTERCARD INTERNAONALCAP%" THEN 'MASTERCARD INTERNATIONAL CAP'
   WHEN c6 LIKE "RETURN SETTLE A ACH RETURN RETURN ITEM SETTLEACH RETURN%" THEN 'ACH RETURN SETTLE'
   WHEN c6 LIKE "TRANSFER TO DEPOSIT ACCOUNT%" THEN 'TRANSFER TO DEPOSIT ACCOUNT'
   WHEN c6 LIKE "TRANSFER TO DEPOSIT SYSTEM ACCOUNT%" THEN 'TRANSFER TO DEPOSIT SYSTEM ACCOUNT'
   WHEN c6 LIKE "TRANSFER FROM DEPOSIT ACCOUNT%" THEN 'TRANSFER FROM DEPOSIT ACCOUNT'
   WHEN c6 LIKE "TRANSFER FROM DEPOSIT SYSTEM ACCOUNT%" THEN 'TRANSFER FROM DEPOSIT SYSTEM ACCOUNT'
   WHEN c6 LIKE "VISA INT'L BASE%" THEN "VISA INT'L BASE II SETTLEMENT"
   ELSE 'somthingelse'
 END AS transactionDetailDescription,
 CASE
   WHEN c6 LIKE 'ACH RETURN%' THEN regexp_extract(c6, '\\b\\d{2}/\\d{2}/\\d{4}\\b', 0)
   WHEN c6 LIKE "AETRS SE PAYABLES NDING ACCTATS%" THEN regexp_extract(c6, "AETRS SE PAYABLES NDING ACCTATS OF (.*) AETRS SE PAYABLE S FUNDI NG ACCT",1)
   WHEN c6 LIKE "AETRS SE PAYABLES NDING ACCTAMERICAN EXPRESS%" THEN 'AETRS SE PAYABLES AMERICAN EXPRESS'
   WHEN c6 LIKE "BANKCARD MERCH FEES%" THEN regexp_extract(c6, "^BANKCARD MERCH FEES (.*) (.*) (.*)$",1)
   WHEN c6 LIKE "BANKCARD DEP BWH - %" THEN regexp_extract(c6, "^BANKCARD DEP BWH - (.*) (.*) (.*)$", 2)
   WHEN c6 LIKE "BANKCARD DEP DAILY DISC%" THEN regexp_extract(c6, "^BANKCARD DEP DA?I?LY DISC? ?S? (.*?) (.*?)$", 1)
   WHEN c6 LIKE "BANKCARD DEP DAILY INT%" THEN regexp_extract(c6, "^BANKCARD DEP DA?I?LY INT ?S? (.*?) (.*?)$", 1)
   WHEN c6 LIKE "BANKCARD DEP MERCH ADJ%" THEN regexp_extract(c6, "^BANKCARD DEP MERCH ADJ (.*) (.*)$", 1)
   WHEN c6 LIKE "BANKCARD DEP MERCH CHBK%" THEN regexp_extract(c6, "^BANKCARD DEP MERCH CHBK (.*) (.*)$", 1)
   WHEN c6 LIKE "BANKCARD DEP MERCH DEP%" THEN regexp_extract(c6, "^BANKCARD DEP MERCH DEP (.*?) (.*?)$", 1)
   WHEN c6 LIKE "DISCOVER FINANCIALERVICES LLC%" THEN regexp_extract(c6, '\\b\\d{2}/\\d{2}/\\d{2}', 0)
   WHEN c6 LIKE "DISCOVER NETWORK SETTLEMENT%" THEN regexp_extract(c6, "^DISCOVER NETWORK SETTLEMENT (.*) (.*)$", 1)
   WHEN c6 LIKE "MASTERCARD INTER MCBS DRAFT%" THEN regexp_extract(c6, "^MASTERCARD INTER MCBS DRAFT (.*) (.*)$", 1)
   WHEN c6 LIKE "MASTERCARD INTERNAONALCAP%" THEN regexp_extract(c6, "^MASTERCARD INTERNAONALCAP OF (.*) MASTERCARD INTERNATION A L$", 1)
            ELSE NULL
 END AS transactionDetailDate,
 CASE
   WHEN c6 LIKE "BANKCARD DEP DAILY DISC%" THEN regexp_extract(c6, "^BANKCARD DEP DA?I?LY DISC? ?S? (.*?) (.*?)$", 2)
   WHEN c6 LIKE "BANKCARD DEP DAILY INT%" THEN regexp_extract(c6, "^BANKCARD DEP DA?I?LY INT ?S? (.*?) (.*?)$", 2)
   WHEN c6 LIKE "BANKCARD DEP MERCH ADJ%" THEN regexp_extract(c6, "^BANKCARD DEP MERCH ADJ (.*) (.*)$", 2)
   WHEN c6 LIKE "BANKCARD DEP MERCH CHBK%" THEN regexp_extract(c6, "^BANKCARD DEP MERCH CHBK (.*) (.*)$", 2)
   WHEN c6 LIKE "BANKCARD DEP MERCH DEP%" THEN regexp_extract(c6, "^BANKCARD DEP MERCH DEP (.*?) (.*?)$", 2)
   WHEN c6 LIKE "DISCOVER NETWORK SETTLEMENT%" THEN regexp_extract(c6, "^DISCOVER NETWORK SETTLEMENT (.*) (.*)$", 2)
   WHEN c6 LIKE "MASTERCARD INTER MCBS DRAFT%" THEN regexp_extract(c6, "^MASTERCARD INTER MCBS DRAFT (.*) (.*)$", 2)
   WHEN c6 LIKE "TRANSFER TO DEPOSIT ACCOUNT%" THEN regexp_extract(c6, "^TRANSFER TO DEPOSIT ACCOUNT (.*)$", 1)
   WHEN c6 LIKE "TRANSFER TO DEPOSIT SYSTEM ACCOUNT%" THEN regexp_extract(c6, "^TRANSFER TO DEPOSIT SYSTEM ACCOUNT (.*)$", 1)
   WHEN c6 LIKE "TRANSFER FROM DEPOSIT ACCOUNT%" THEN regexp_extract(c6, "^TRANSFER FROM DEPOSIT ACCOUNT (.*)$", 1)
   WHEN c6 LIKE "TRANSFER FROM DEPOSIT SYSTEM ACCOUNT%" THEN regexp_extract(c6, "^TRANSFER FROM DEPOSIT SYSTEM ACCOUNT (.*)$", 1)
   WHEN c6 LIKE "VISA INT'L BASE%" THEN regexp_extract(c6, "^VISA INT'L BASE IIETTLEMENT(.*) VSS SETTLEMENT OVISA.*$", 1)
   ELSE NULL
 END AS transactionDetailAccount,
 CASE
   WHEN c6 LIKE "BANKCARD DEP DAILY DISC%" THEN regexp_extract(c6, "^BANKCARD DEP DA?I?LY DISC? ?S? (.*?) (.*?) (.*)$", 3)
   WHEN c6 LIKE "BANKCARD DEP DAILY INT%" THEN regexp_extract(c6, "^BANKCARD DEP DA?I?LY INT ?S? (.*?) (.*?) (.*)$", 3)
   WHEN c6 LIKE "BANKCARD DEP MERCH ADJ%" THEN regexp_extract(c6, "^BANKCARD DEP MERCH ADJ (.*) (.*) (.*)$", 3)
   WHEN c6 LIKE "BANKCARD DEP MERCH CHBK%" THEN regexp_extract(c6, "^BANKCARD DEP MERCH CHBK (.*) (.*) (.*)$", 3)
   WHEN c6 LIKE "BANKCARD DEP MERCH DEP%" THEN regexp_extract(c6, "^BANKCARD DEP MERCH DEP (.*?) (.*?) (.*)$", 3)
   ELSE NULL
 END AS transactionDetailNumber
FROM BankActivities_16
`;
  const code5=`
SELECT
 f.*,
 g.*,
 a.*,
 t.*
FROM df_files_mapped f
LEFT JOIN df_groups_mapped g
 USING (group_id01)
LEFT JOIN df_accounts_mapped a
 USING (group_id02)
 LEFT JOIN df_transactions_mapped t
 USING (group_id03)
`;

  const etl = {
    id: 2,
    name: 'Raw file from S3 to Kafka topic',
    status: 'ACTIVE',
    activeVersion: 'Live: 3',
    // scheduled: 'Trigger by a cronjob every 8 hours',
    trigger: {
      type: 'when file uploaded'
    },
    stages: [
      {
        index: 1,
        type: 'PARSER',
        fileFormat: 'BAI',
        viewName: 'BankActivities_${type}',
        isOutput: false,
      },
      {
        index: 2,
        type: 'SQL',
        code: code1,
        isOutput: false,
      },
      {
        index: 3,
        type: 'SQL',
        code: code2,
        isOutput: false,
      },
      {
        index: 4,
        type: 'SQL',
        code: code3,
        isOutput: false,
      },
      {
        index: 5,
        type: 'SQL',
        code: code4,
        isOutput: false,
      },
      {
        index: 6,
        type: 'SQL',
        code: code5,
        isOutput: true,
        outputName: 'BankActivities'
      }
    ],
    jobHistory: [
      {
        title: 'Sync Succeeded',
        date: '10:00AM 02/27/2024',
        detail: '500 records extracted | 500 records loaded | JobId: 123 | 17s',
        jobId: 123,
        results: [
          {
            stage: 'Stage #1',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #2',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #3',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #4',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #5',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #6: BankActivies',
            detail: '500 records loaded',
          },
        ]
      },
      {
        title: 'Sync Succeeded',
        date: '11:00AM 02/28/2024',
        detail: '200 records extracted | 200 records loaded | JobId: 456 | 17s',
        jobId: 456,
        results: [
          {
            stage: 'Stage #1',
            detail: '100 records extracted from S3',
          },
          {
            stage: 'Stage #2',
            detail: '100 records extracted from S3',
          },
          {
            stage: 'Stage #3',
            detail: '100 records extracted from S3',
          },
          {
            stage: 'Stage #4',
            detail: '100 records extracted from S3',
          },
          {
            stage: 'Stage #5',
            detail: '100 records extracted from S3s',
          },
          {
            stage: 'Stage #6: BankActivies',
            detail: '500 records loaded',
          },
        ]
      },
      {
        title: 'Sync Failed',
        date: '11:00AM 02/29/2024',
        detail: '0 records extracted | 0 records loaded | JobId: 789 | 2m',
        jobId: 789,
        results: [
          {
            stage: 'Stage #1',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #2',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #3',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #4',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #5',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #6: BankActivies',
            detail: '500 records loaded',
          },
        ]
      },
      {
        title: 'Sync Succeeded',
        date: '11:00AM 02/28/2024',
        detail: '200 records extracted | 200 records loaded | JobId: 0001 | 17s',
        jobId: "0001",
        results: [
          {
            stage: 'Stage #1',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #2',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #3',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #4',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #5',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #6: BankActivies',
            detail: '500 records loaded',
          },
        ]
      }
    ],
    source: {
      name: 'mxc raw files - S3',
      type: 'FILE_S3',
      filePattern: 'BankActivity*.bai',
      bucket: 'mxc-raw-files',
    },
    destinations: [
      {
        stage_index: 2,
        stage_name: 'details data',
        name: 'Kafka streaming test',
        type: 'KAFKA',
        topic: 'bank_activity',
      },
    ],
    versions: [
      {
        version: '3.0',
        date: '02/17/2024',
        description: 'by Jhonny Córdova at 02/17/2024 10:00AM',
        status: 'PUBLISHED',
        active: true
      },
      {
        version: '2.0',
        date: '02/12/2024',
        description: 'since 02/12/2024 10:00AM until 02/15/2024 10:00AM',
        status: 'PUBLISHED',
        active: false
      }, 
      {
        version: '1.0',
        date: '02/10/2024',
        description: 'last modified by Jhonny Córdova at 02/10/2024 10:00AM',
        status: 'DRAFT',
        active: false
      },
    ],
    changes: [
      {
        date: '02/27/2024 12:00:00',
        action: 'created',
        user: 'Jhonny Córdova',
      },
      {
        date: '02/29/2024 12:00:00',
        action: 'published',
        user: 'Jhonny Córdova',
      },
    ]
  };

  useEffect(() => {
    Prism.highlightAll();
  }, []);

  return (
    <>
      <EtlView etl={etl} />
    </>
  );
}
