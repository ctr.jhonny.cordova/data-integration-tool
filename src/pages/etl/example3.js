import Prism from "prismjs";
import "prismjs/components/prism-sql";
import "prismjs/themes/prism.css";
import { useEffect } from 'react';

import EtlView from "../../components/etlView";

export default function Page() {

  const code2 = `

  function handleRow(row) {
    const columns = row.split(',');
    const date = columns[0];
    const amount = columns[1];
    const currency = columns[2];
    const description = columns[3];
    const category = columns[4];

    return {
      date,
      amount,
      currency,
      description,
      category
    };
  }

`;

const sqlCode = `
CREATE VIEW FilteredSalesSummary AS
  SELECT 
    Customers.CustomerName, 
    Products.ProductName, 
    SUM(OrderDetails.Quantity) AS TotalQuantity, 
    SUM(OrderDetails.Quantity * Products.Price) AS TotalSales
  FROM Customers
  INNER JOIN Orders ON Customers.CustomerID = Orders.CustomerID
  INNER JOIN OrderDetails ON Orders.OrderID = OrderDetails.OrderID
  INNER JOIN Products ON OrderDetails.ProductID = Products.ProductID
  WHERE Customers.Country = 'USA' AND Orders.OrderDate >= '2022-01-01'
  GROUP BY Customers.CustomerName, Products.ProductName
  ORDER BY TotalSales DESC;
`;

  const etl = {
    name: 'HDFS file as source example',
    status: 'ACTIVE',
    activeVersion: 'Version 3',
    // scheduled: 'Trigger by a cronjob every 8 hours',
    trigger: {
      type: 'when file uploaded'
    },
    stages: [
      {
        index: 1,
        type: 'REGEX_FILE',
        Regex: '.*payments.*\\.csv',
        // fileFormat: 'CSV',
        // recordDelimiter: '\\n',
        // viewName: 'test_view',
        isOutput: false,
      },
      {
        index: 2,
        type: 'SIMPLE',
        code: code2,
        isOutput: true,
        outputName: 'simple transformation'
      },
      {
        index: 3,
        type: 'SQL',
        code: sqlCode,
        isOutput: true,
        outputName: 'final modifications to MySQL destination'
      },
    ],
    jobHistory: [
      {
        title: 'Sync Succeeded',
        date: '10:00AM 02/27/2024',
        detail: '100 records extracted | 100 records loaded | JobId: 123 | 17s',
        jobId: 123,
        results: [
          {
            stage: 'Stage #1',
            detail: '100 records extracted',
          },
          {
            stage: 'Stage #: sales_view',
            detail: '100 records loaded',
          }
        ]
      },
      {
        title: 'Sync Succeeded',
        date: '11:00AM 02/28/2024',
        detail: '200 records extracted | 200 records loaded | JobId: 456 | 17s',
        jobId: 456,
        results: [
          {
            stage: 'Stage #1',
            detail: '200 records extracted',
          },
          {
            stage: 'Stage #: sales_view',
            detail: '200 records loaded',
          }
        ]
      },
      {
        title: 'Sync Failed',
        date: '11:00AM 02/29/2024',
        detail: '0 records extracted | 0 records loaded | JobId: 789 | 2m',
        jobId: 789,
        results: [
          {
            stage: 'Stage #1',
            detail: '0 records extracted',
          },
          {
            stage: 'Stage #: sales_view',
            detail: '0 records loaded',
          }
        ]
      },
      {
        title: 'Sync Succeeded',
        date: '11:00AM 02/28/2024',
        detail: '200 records extracted | 200 records loaded | JobId: 0001 | 17s',
        jobId: "0001",
        results: [
          {
            stage: 'Stage #1',
            detail: '200 records extracted',
          },
          {
            stage: 'Stage #: sales_view',
            detail: '200 records loaded',
          }
        ]
      }
    ],
    source: {
      name: 'HDFS_FILE - HDFS',
      type: 'FILE_HDFS',
      connectionUrl: 'hdfs://sandbox-hdp.hortonworks.com:8020',
      // bucket: 'mxc-raw-files/',
    },
    destinations: [
      {
        stage_index: 2,
        stage_name: 'simple transformation',
        name: 'Kafka streaming test',
        type: 'KAFKA',
        topic: 'payments_test',
      },
      {
        stage_index: 3,
        stage_name: 'final modifications to MySQL destination',
        name: 'PRTH MySQL',
        type: 'MYSQL',
        tableName: 'payments_table',
      }
    ],
    versions: [
      {
        version: '3.0',
        date: '02/17/2024',
        description: 'by Jhonny Córdova at 02/17/2024 10:00AM',
        status: 'PUBLISHED',
        active: true
      },
      {
        version: '2.0',
        date: '02/12/2024',
        description: 'since 02/12/2024 10:00AM until 02/15/2024 10:00AM',
        status: 'PUBLISHED',
        active: false
      }, 
      {
        version: '1.0',
        date: '02/10/2024',
        description: 'last modified by Jhonny Córdova at 02/10/2024 10:00AM',
        status: 'DRAFT',
        active: false
      },
    ],
    changes: [
      {
        date: '02/27/2024 12:00:00',
        action: 'created',
        user: 'Jhonny Córdova',
      },
      {
        date: '02/29/2024 12:00:00',
        action: 'published',
        user: 'Jhonny Córdova',
      },
    ]
  };

  useEffect(() => {
    Prism.highlightAll();
  }, []);

  return (
    <>
      <EtlView etl={etl} />
    </>
  );
}
