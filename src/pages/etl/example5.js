import Prism from "prismjs";
import "prismjs/components/prism-sql";
import "prismjs/themes/prism.css";
import { useEffect } from 'react';

export default function Page() {
  const code = `
function processPayments(payments) {
  const transformedPayments = payments.map(payment => {
    return {
      id: payment.id,
      amount: payment.amount,
      date: payment.date,
      status: payment.status
    }
  }
  `;
  useEffect(() => {
    Prism.highlightAll();
  }, [code]);
  return (
    <div>
      <h2 className="mb-4">ETL with HDFS as source</h2>
      <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li className="nav-item" role="presentation">
          <button className="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Status</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Job History</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Source</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-transformation-tab" data-bs-toggle="pill" data-bs-target="#pills-transformation" type="button" role="tab" aria-controls="pills-transformation" aria-selected="false">Transformation</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-destination-tab" data-bs-toggle="pill" data-bs-target="#pills-destination" type="button" role="tab" aria-controls="pills-destination" aria-selected="false">Destination</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-versions-tab" data-bs-toggle="pill" data-bs-target="#pills-versions" type="button" role="tab" aria-controls="pills-versions" aria-selected="false">Versions</button>
        </li>
        <li className="nav-item" role="presentation">
          <button className="nav-link" id="pills-test-tab" data-bs-toggle="pill" data-bs-target="#pills-test" type="button" role="tab" aria-controls="pills-test" aria-selected="false">Test ETL</button>
        </li>
      </ul>
      <div className="tab-content" id="pills-tabContent">
        <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
          <div className="card bg-gray-300 shadow-none mb-4">
            <div className="card-body">
              <div className="d-flex justify-content-between
                      align-items-center">
                <div>
                  <h6 className="mb-3">Trigger by a cronjob every 24 hours</h6>
                  <h6 className="mb-0">Published - Version 3</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" tabindex="0">
          <div className="mb-8">

            <div className="card bg-gray-300 shadow-none mb-4">

              <div className="card-body">
                <div className="d-flex justify-content-between
                        align-items-center">
                  <div>
                    <h6 className="mb-0">Job History</h6>
                  </div>
                  <div>
                    <button type="button" class="btn btn-success btn-sm">Run now</button>
                  </div>
                </div>
              </div>
            </div>

            <div className="card">

              <ul className="list-group list-group-flush">

                <li className="list-group-item p-3">
                  <div className="d-flex justify-content-between
                          align-items-center">
                    <div className="d-flex align-items-center">

                      <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="green" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
                          <path d="M20 6L9 17l-5-5"></path>
                        </svg>
                      </div>

                      <div className="ms-3">
                        <p className="mb-0
                                font-weight-medium">[10:00AM 02/27/2024] Sync Succeeded</p>
                        <p className="mb-0 font-weight-medium fw-lighter">100 records extracted | 100 records loaded | JobId: 123 | 17s</p>
                      </div>
                    </div>
                    <div>

                      <div className="dropdown dropstart">
                        <a href="#!" className="btn btn-ghost btn-icon btn-sm rounded-circle" id="dropdownactivityOne" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical icon-xs"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                        </a>
                        <div className="dropdown-menu" aria-labelledby="dropdownactivityOne">
                          <a className="dropdown-item d-flex align-items-center" href="#!">Action</a>
                          <a className="dropdown-item d-flex align-items-center" href="#!">Another action</a>
                          <a className="dropdown-item d-flex align-items-center" href="#!">Something else
                            here</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div className="card">

              <ul className="list-group list-group-flush">

                <li className="list-group-item p-3">
                  <div className="d-flex justify-content-between
                          align-items-center">
                    <div className="d-flex align-items-center">

                      <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="green" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
                          <path d="M20 6L9 17l-5-5"></path>
                        </svg>
                      </div>

                      <div className="ms-3">
                        <p className="mb-0
                                font-weight-medium">[11:00AM 02/28/2024] Sync Succeeded</p>
                        <p className="mb-0 font-weight-medium fw-lighter">200 records extracted | 200 records loaded | JobId: 123 | 17s</p>
                      </div>
                    </div>
                    <div>

                      <div className="dropdown dropstart">
                        <a href="#!" className="btn btn-ghost btn-icon btn-sm rounded-circle" id="dropdownactivityOne" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical icon-xs"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                        </a>
                        <div className="dropdown-menu" aria-labelledby="dropdownactivityOne">
                          <a className="dropdown-item d-flex align-items-center" href="#!">Action</a>
                          <a className="dropdown-item d-flex align-items-center" href="#!">Another action</a>
                          <a className="dropdown-item d-flex align-items-center" href="#!">Something else
                            here</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div className="card">

              <ul className="list-group list-group-flush">

                <li className="list-group-item p-3">
                  <div className="d-flex justify-content-between
                          align-items-center">
                    <div className="d-flex align-items-center">

                      <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="red" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                          <path d="M18 6L6 18M6 6l12 12"></path>
                        </svg>
                      </div>

                      <div className="ms-3">
                        <p className="mb-0
                                font-weight-medium">[11:00AM 02/29/2024] Sync Failed</p>
                        <p className="mb-0 font-weight-medium fw-lighter">0 records extracted | 0 records loaded | JobId: 123 | 2m</p>
                      </div>
                    </div>
                    <div>

                      <div className="dropdown dropstart">
                        <a href="#!" className="btn btn-ghost btn-icon btn-sm rounded-circle" id="dropdownactivityOne" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical icon-xs"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                        </a>
                        <div className="dropdown-menu" aria-labelledby="dropdownactivityOne">
                          <a className="dropdown-item d-flex align-items-center" href="#!">Action</a>
                          <a className="dropdown-item d-flex align-items-center" href="#!">Another action</a>
                          <a className="dropdown-item d-flex align-items-center" href="#!">Something else
                            here</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div className="card">

              <ul className="list-group list-group-flush">

                <li className="list-group-item p-3">
                  <div className="d-flex justify-content-between
                          align-items-center">
                    <div className="d-flex align-items-center">

                      <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="green" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
                          <path d="M20 6L9 17l-5-5"></path>
                        </svg>
                      </div>

                      <div className="ms-3">
                        <p className="mb-0
                                font-weight-medium">[11:00AM 02/28/2024] Sync Succeeded</p>
                        <p className="mb-0 font-weight-medium fw-lighter">200 records extracted | 200 records loaded | JobId: 123 | 17s</p>
                      </div>
                    </div>
                    <div>

                      <div className="dropdown dropstart">
                        <a href="#!" className="btn btn-ghost btn-icon btn-sm rounded-circle" id="dropdownactivityOne" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical icon-xs"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                        </a>
                        <div className="dropdown-menu" aria-labelledby="dropdownactivityOne">
                          <a className="dropdown-item d-flex align-items-center" href="#!">Action</a>
                          <a className="dropdown-item d-flex align-items-center" href="#!">Another action</a>
                          <a className="dropdown-item d-flex align-items-center" href="#!">Something else
                            here</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab" tabindex="0">
          <div className="card bg-gray-300 shadow-none mb-4">
            <div className="card-body">
              <div className="d-flex justify-content-between
                      align-items-center">
                <div className="container">

                  <div className="row">
                    <div className="col-6">
                      <h3 className="mb-3">HDFS file example</h3>
                      <h5 className="mb-3">Production</h5>
                      <h6 className="mb-3">Source Details</h6>

                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Connection URL</strong>
                        </label>
                        <div>199.292.222.222/example/example</div>
                      </div>

                    </div>
                    <div className="col-6">
                      <h3 className="mb-3">&nbsp;</h3>
                      <h3 className="mb-3">Testing</h3>
                      <h6 className="mb-3">Source Details</h6>

                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Connection URL</strong>
                        </label>
                        <div>199.292.222.221/test/test</div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tab-pane fade show" id="pills-transformation" role="tabpanel" aria-labelledby="pills-transformation-tab" tabindex="0">
          <div className="card bg-gray-300 shadow-none mb-4">
            <div className="card-body">
              <div className="d-flex justify-content-between
                      align-items-center">
                <div className="container">

                  <div className="row">
                    <div className="col-8">
                      <h6 className="mb-3">Transformation details</h6>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Type</strong>
                        </label>
                        <div>Simple</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Source</strong>
                        </label>
                        <div>GIT</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>GIT URL</strong>
                        </label>
                        <div>https://github.com/cordovj/test/blob/master/payments_recon.js</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>JS Code from Git</strong>
                        </label>
                        <div className="Code">
                          <pre>
                            <code className={`language-sql`}>{code}</code>
                          </pre>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tab-pane fade show" id="pills-destination" role="tabpanel" aria-labelledby="pills-destination-tab" tabindex="0">
          <div className="card bg-gray-300 shadow-none mb-4">
            <div className="card-body">
              <div className="d-flex justify-content-between
                      align-items-center">
                <div className="container">

                  <div className="row">
                    <div className="col-6">
                      <h3 className="mb-3">PRTH MySQL</h3>
                      <h4 className="mb-3">Production</h4>
                      <h6 className="mb-3">Destination details</h6>

                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Host</strong>
                        </label>
                        <div>prth-mysql-rds-snowflake.com</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Host</strong>
                        </label>
                        <div>3306</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Database</strong>
                        </label>
                        <div>PRTH_DB</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Username</strong>
                        </label>
                        <div>app_user_db</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Password</strong>
                        </label>
                        <div>******</div>
                      </div>
                      <h6 className="mb-3">Specify details for this connection</h6>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Table Name</strong>
                        </label>
                        <div>payments</div>
                      </div>
                    </div>
                    <div className="col-6">
                      <h3 className="mb-3">&nbsp;</h3>
                      <h4 className="mb-3">Testing</h4>
                      <h6 className="mb-3">Destination details</h6>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Host</strong>
                        </label>
                        <div>prth-mysql-TESTING-rds-snowflake.com</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Host</strong>
                        </label>
                        <div>3306</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Database</strong>
                        </label>
                        <div>PRTH_DB</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Username</strong>
                        </label>
                        <div>app_user_db</div>
                      </div>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Password</strong>
                        </label>
                        <div>******</div>
                      </div>
                      <h6 className="mb-3">Specify details for this connection</h6>
                      <div className="mb-3">
                        <label className="form-label">
                          <strong>Table Name</strong>
                        </label>
                        <div>payments</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tab-pane fade" id="pills-versions" role="tabpanel" aria-labelledby="pills-versions-tab" tabindex="0">
          <div className="mb-8">

            <div className="card bg-gray-300 shadow-none mb-4">

              <div className="card-body">
                <div className="d-flex justify-content-between
                        align-items-center">
                  <div>
                    <h6 className="mb-0">Versions</h6>
                  </div>
                </div>
              </div>
            </div>

            <div className="card">

              <ul className="list-group list-group-flush">

                <li className="list-group-item p-3">
                  <div className="d-flex justify-content-between
            align-items-center">
                    <div className="d-flex align-items-center">

                      <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="green" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
                          <path d="M20 6L9 17l-5-5"></path>
                        </svg>
                      </div>

                      <div className="ms-3">
                        <p className="mb-0
                  font-weight-medium">[Created at 10:00AM 02/27/2024] Version 3</p>
                        <p className="mb-0 font-weight-medium fw-lighter">Published at 10/12/2023 03:30:00 UTC</p>
                      </div>
                    </div>
                    <div>

                      <div className="dropdown dropstart">
                        <a href="#!" className="btn btn-ghost btn-icon btn-sm rounded-circle" id="dropdownactivityOne" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical icon-xs"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                        </a>
                        <div className="dropdown-menu" aria-labelledby="dropdownactivityOne">
                          <a className="dropdown-item d-flex align-items-center" href="#!">Clone this version</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div className="card">

              <ul className="list-group list-group-flush">

                <li className="list-group-item p-3">
                  <div className="d-flex justify-content-between
                          align-items-center">
                    <div className="d-flex align-items-center">

                      <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="green" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
                          <path d="M20 6L9 17l-5-5"></path>
                        </svg>
                      </div>

                      <div className="ms-3">
                        <p className="mb-0
                                font-weight-medium">[Created at 10:00AM 02/27/2024] Version 2</p>
                        <p className="mb-0 font-weight-medium fw-lighter">Published at 10/05/2023 03:30:00 UTC</p>
                      </div>
                    </div>
                    <div>

                      <div className="dropdown dropstart">
                        <a href="#!" className="btn btn-ghost btn-icon btn-sm rounded-circle" id="dropdownactivityOne" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical icon-xs"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                        </a>
                        <div className="dropdown-menu" aria-labelledby="dropdownactivityOne">
                          <a className="dropdown-item d-flex align-items-center" href="#!">Clone this version</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div className="card">

              <ul className="list-group list-group-flush">

                <li className="list-group-item p-3">
                  <div className="d-flex justify-content-between
                          align-items-center">
                    <div className="d-flex align-items-center">

                      <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="green" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
                          <path d="M20 6L9 17l-5-5"></path>
                        </svg>
                      </div>

                      <div className="ms-3">
                        <p className="mb-0
                                font-weight-medium">[Created at 10:00AM 02/27/2024] Version 1</p>
                        <p className="mb-0 font-weight-medium fw-lighter">Published at 10/01/2023 03:30:00 UTC</p>
                      </div>
                    </div>
                    <div>

                      <div className="dropdown dropstart">
                        <a href="#!" className="btn btn-ghost btn-icon btn-sm rounded-circle" id="dropdownactivityOne" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-more-vertical icon-xs"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                        </a>
                        <div className="dropdown-menu" aria-labelledby="dropdownactivityOne">
                          <a className="dropdown-item d-flex align-items-center" href="#!">Clone this version</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="tab-pane fade show" id="pills-test" role="tabpanel" aria-labelledby="pills-test-tab" tabindex="0">
          <div className="card bg-gray-300 shadow-none mb-4">
            <div className="card-body">
              <div className="d-flex justify-content-between
                      align-items-center">
                <div>
                  <h6 className="mb-3">WIP</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div >
  );
}