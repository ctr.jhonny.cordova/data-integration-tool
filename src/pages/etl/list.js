import Link from 'next/link';
import { useState } from 'react';

export default function Page() {

  const [live, setLive] = useState(true);

  function handleLiveChange(e) {
    setLive(e.target.checked);
  }

  return (
    <div>
      <h1 className="h3 mb-3">ETL processes</h1>
      <div className="d-flex mb-4 justify-content-end">
        <div className="row">
          <div className="col-auto">
            <label className="col-form-label">Name</label>
          </div>
          <div className="col-auto">
            <input type="text" className="form-control" name='name' onChange={(e) => { }} />
          </div>

          <div className="col-auto">
            <div className="form-check form-switch mt-2 me-5">
              <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked={live} onChange={handleLiveChange} />
              <label className="form-check-label" for="flexSwitchCheckChecked">Live</label>
            </div>
          </div>

          <div className="col-auto">
            <button type="button" onClick={() => { }} className="btn btn-primary">Search</button>
          </div>
          <br />
        </div>

      </div>
      <table className="table align-middle mb-0 bg-white">
        <thead className="bg-light">
          <tr>
            <th>Name</th>
            <th>Route</th>
            {/* <th>Destination Name</th> */}
            <th>Trigger</th>
            <th>Active version</th>
            <th>Rows last 30 days</th>
            <th>Last Sync</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div className="d-flex align-items-center">
                <div>
                  <Link href={'/etl/example1'} className="link-dark rounded">
                    MySQL Server to PRTH MySQL (Process 1)
                  </Link>
                </div>
              </div>
            </td>
            <td>
              <p className="fw-normal mb-1">MySQL Server → PRTH MySQL</p>
            </td>
            {/* <td>
              PRTH MySQL
            </td> */}
            <td>
              Manual
            </td>
            <td>
              1
            </td>
            <td>
              None
            </td>
            <td>
              2021-08-31 12:00:00
            </td>
            <td>
              LIVE
              {/* <div className="form-check form-switch">
                <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" />
              </div> */}
            </td>
          </tr>
          <tr>
            <td>
              <div className="d-flex align-items-center">
                <div>
                  <Link href={'/etl/example2'} className="link-dark rounded">
                    Raw file from S3 to Kafka topic
                  </Link>
                </div>
              </div>
            </td>
            <td>
              <p className="fw-normal mb-1">mxc raw files - S3 → Kafka topic</p>
            </td>
            {/* <td>
              Kafka topic
            </td> */}
            <td>
              File uploaded
            </td>
            <td>
              3
            </td>
            <td>
              1582
            </td>
            <td>
              2021-08-31 12:00:00
            </td>
            <td>
              LIVE
              {/* <div className="form-check form-switch">
                <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked />
              </div> */}
            </td>
          </tr>
          <tr>
            <td>
              <div className="d-flex align-items-center">
                <div>
                  <Link href={'/etl/example3'} className="link-dark rounded">
                    HDFS file as source example
                  </Link>
                </div>
              </div>
            </td>
            <td>
              <p className="fw-normal mb-1">HDFS file example → Kafka streaming test</p>
            </td>
            {/* <td>
            Kafka streaming test
            </td> */}
            <td>
              24 hours
            </td>
            <td>
              4
            </td>
            <td>
              1000
            </td>
            <td>
              2021-08-31 12:00:00
            </td>
            <td>
              LIVE
              {/* <div className="form-check form-switch">
                <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked />
              </div> */}
            </td>
          </tr>
          <tr>
            <td>
              <div className="d-flex align-items-center">
                <div>
                  <Link href={'/etl/example4'} className="link-dark rounded">
                    SFTP file to kafka topic
                  </Link>
                </div>
              </div>
            </td>
            <td>
              <p className="fw-normal mb-1">SFTP file example → Kafka streaming test</p>
            </td>
            {/* <td>
            Kafka streaming test
            </td> */}
            <td>
              File uploaded
            </td>
            <td>
              3
            </td>
            <td>
              567
            </td>
            <td>
              2021-08-31 12:00:00
            </td>
            <td>
              REHEARSAL
            </td>
          </tr>
        </tbody>
      </table>
      <br />
    </div>
  );
}

