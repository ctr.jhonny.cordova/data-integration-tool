
import { useEffect, useState } from 'react';
import { Wizard, useWizard } from 'react-use-wizard';
import ace from 'ace-builds/src-noconflict/ace';
import 'ace-builds/src-noconflict/mode-javascript';
import 'ace-builds/src-noconflict/mode-mysql';
import 'ace-builds/src-noconflict/theme-monokai';
import PreviewExample from './preview';
import { set } from 'ace-builds/src-noconflict/ace';
import dynamic from 'next/dynamic';

const FileBrowser = dynamic(() => import('../../components/fileBrowser'), {
  ssr: false,
})

const AceEditor = dynamic(() => import('react-ace'), {
  ssr: false,
})


export default function Page() {
  const [step, setStep] = useState(0);
  const [etl, setEtl] = useState({ sourceType: 'FILE_S3', stages: [{ index: 0, type: 'PARSER' }], destinations: [], versionName: '1'});
  const handleStepChange = (step) => {
    setStep(step);
  }

  const [source, setSource] = useState('FILE_S3');
  const handleSourceChange = (source) => {
    setSource(source);
    setEtl({ ...etl, sourceType: source });
  }

  const classNameDs = ['breadcrumb-item', step === 0 ? 'active' : ""].join(' ')
  // const classNameDt = ['breadcrumb-item', step === 1 ? 'active' : ""].join(' ')
  const classNameT = ['breadcrumb-item', step === 1 ? 'active' : ""].join(' ')
  const classNameDd = ['breadcrumb-item', step === 2 ? 'active' : ""].join(' ')

  return (
    <div>
      <h2 className="my-4">
        Create ETL
      </h2>
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb">
          <li className={classNameDs}>Defining Source</li>
          {/* <li className={classNameDt}>Defining Trigger</li> */}
          <li className={classNameT}>Transformation</li>
          <li className={classNameDd}>Defining Destination</li>
        </ol>
      </nav>
      <Wizard onStepChange={handleStepChange}>
        <Step1 source={source} changeSource={handleSourceChange} etl={etl} setEtl={setEtl} />
        <Step2 etl={etl} setEtl={setEtl} />
        <Step3 etl={etl} setEtl={setEtl} />
      </Wizard>
    </div>
  );
}

const CodeEditor = ({ index, etl, setEtl }) => {
  const stage = etl.stages.find(stage => stage.index === index);
  const [code, setCode] = useState(stage.code || '');

  const handleChange = (newCode) => {
    setCode(newCode);
    setEtl({
      ...etl, stages: etl.stages.map(stage => {
        if (stage.index === index) {
          return { ...stage, code: newCode };
        }
        return stage;
      })
    });
  };

  return (
    <div>
      <AceEditor
        mode="mysql"
        // theme="monokai"
        onChange={handleChange}
        value={code}
        name="code-editor"
        editorProps={{ $blockScrolling: true }}
        width="100%"
        height="300px"
      />
      {/* <div>
        <h2>Your Code:</h2>
        <pre>{code}</pre>
      </div> */}
    </div>
  );
};

function MySQLForm({ isDestination = false }) {
  const [code, setCode] = useState('');
  const [syncType, setSyncType] = useState('QUERY');

  const handleChange = (newCode) => {
    setCode(newCode);
  };

  const handleSyncTypeChange = (e) => {
    setSyncType(e.target.value);
  }

  return (
    <div>

      {isDestination && (
        <>
          <p className="fw-bold">Details</p>
          <div className="mb-3">
            <label htmlFor={'tableName'} className="form-label">Table name</label>
            <input type="text" className="form-control" name="tableName" id="tableName" />
          </div>
        </>

      )}
      {!isDestination && (
        <>
          <p className="fw-bold">Details</p>
          <hr />
          <div className="mb-3">
            <label htmlFor={'syncType'} className="form-label">Sync Type</label>
            <select className="form-select" name="syncType" id="syncType" value={syncType} onChange={handleSyncTypeChange}>
              <option key="1" value="QUERY">Query</option>
              <option key="2" value="BINLOG">BinLog</option>
            </select>
          </div>

          {syncType === 'QUERY' && (
            <div className="mb-3">
              <label htmlFor={'tableName'} className="form-label">SQL code</label>
              <AceEditor
                mode="mysql"
                // theme="monokai"
                onChange={handleChange}
                value={code}
                name="code-editor"
                editorProps={{ $blockScrolling: true }}
                width="100%"
                height="300px"
              />
            </div>
          )}

          {syncType === 'BINLOG' && (
            <div>
              <label htmlFor={'tableName'} className="form-label">Tables</label>
              <div className="form-check">
                <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                <label className="form-check-label" for="flexCheckDefault">
                  payment_capability.t_payment_info
                </label>
              </div>
              <div className="form-check">
                <input className="form-check-input" type="checkbox" value="" id="flexCheckChecked" />
                <label className="form-check-label">
                  payment_capability.t_payment
                </label>
              </div>
              <div className="form-check mb-3">
                <input className="form-check-input" type="checkbox" value="" id="flexCheckChecked" />
                <label className="form-check-label">
                  payment_capability.t_staged_payment
                </label>
              </div>
            </div>
          )}
        </>
      )}


    </div>
  );
}

function SnowflakeForm() {
  const [code, setCode] = useState('');

  const handleChange = (newCode) => {
    setCode(newCode);
  };
  return (
    <div>
      <p className="fw-bold">Details</p>
      <hr />
      <div className="mb-3">
        <label htmlFor={'schema'} className="form-label">Schema</label>
        <input type="text" className="form-control" name="schema" id="schema" />
      </div>
      <div className="mb-3">
        <label htmlFor={'tableName'} className="form-label">Table name</label>
        <input type="text" className="form-control" name="tableName" id="tableName" />
      </div>
      <div className="mb-3">
        <label htmlFor={'tableName'} className="form-label">SQL code</label>
        <AceEditor
          mode="mysql"
          // theme="monokai"
          onChange={handleChange}
          value={code}
          name="code-editor"
          editorProps={{ $blockScrolling: true }}
          width="100%"
          height="300px"
        />
      </div>
    </div>
  );
}

function FileForm({ type, etl, setEtl, forStageIndex }) {
  const [bucket, setBucket] = useState(forStageIndex ? etl.destinations.find(destination => destination.index === forStageIndex).bucket : etl.bucket || '');
  const handleChangeFormat = (e) => {
    setFormat(e.target.value);
  }

  const handleBucketChange = (e) => {
    // setEtl({ ...etl, bucket: e.target.value });
    if (forStageIndex !== undefined) {
      setEtl({
        ...etl, destinations: etl.destinations.map(destination => {
          if (destination.index === forStageIndex) {
            return { ...destination, bucket: e.target.value };
          }
          return destination;
        }
        )
      });
    } else {
      setEtl({ ...etl, bucket: e.target.value });
    }
    setBucket(e.target.value);
  }

  return (
    <div>
      <p className="fw-bold">Details</p>
      <hr />
      {type === 'FILE_S3' && (
        <>
          <div className="mb-3">
            <label htmlFor={'bucket'} className="form-label">Bucket</label>
            <input type="text" className="form-control" name="bucket" id="bucket" value={bucket} onChange={handleBucketChange} />
          </div>
          <div className="mb-3">
            <label htmlFor={'file_pattern'} className="form-label">File Pattern</label>
            <input type="text" className="form-control" name="file_pattern" id="file_pattern" />
          </div>
        </>
      )}

    </div>
  );
}


function KafkaForm({ etl, setEtl, forStageIndex }) {
  const [topic, setTopic] = useState(forStageIndex ? etl.destinations.find(destination => destination.index === forStageIndex).topic : etl.topic || '');

  const handleTopicChange = (e) => {
    // setEtl({ ...etl, bucket: e.target.value });
    if (forStageIndex !== undefined) {
      setEtl({
        ...etl, destinations: etl.destinations.map(destination => {
          if (destination.index === forStageIndex) {
            return { ...destination, bucket: e.target.value };
          }
          return destination;
        }
        )
      });
    } else {
      setEtl({ ...etl, bucket: e.target.value });
    }
    setTopic(e.target.value);
  }

  return (
    <div>
      <p className="fw-bold">Details</p>
      <hr />
      <div className="mb-3">
        <label htmlFor={'topic'} className="form-label">Topic</label>
        <input type="text" className="form-control" name="topic" id="topic" value={topic} onChange={handleTopicChange} />
      </div>
    </div>
  );
}

const Step1 = ({ source, changeSource, etl, setEtl }) => {
  const { handleStep, previousStep, nextStep } = useWizard();

  const handleSourceChange = (e) => {
    changeSource(e.target.value);
  }

  const handleEtlNameChange = (e) => {
    setEtl({ ...etl, name: e.target.value });
  }

  const handleVersionNameChange = (e) => {
    setEtl({ ...etl, versionName: e.target.value });
  }

  return (
    <>
      <div className="row">
        <div className="col-8">
          <form onSubmit={() => { }} noValidate>

          <div className="mb-3">
            <label htmlFor={'etlName'} className="form-label"><strong>ETL name</strong></label>
            <input type="text" className="form-control" name="etlName" id="etlName" value={etl.name} onChange={handleEtlNameChange}/>
          </div>
          <div className="mb-3">
            <label htmlFor={'versionName'} className="form-label"><strong>Version name</strong></label>
            <input type="text" className="form-control" name="versionName" id="versionName" value={etl.versionName} onChange={handleVersionNameChange}/>
          </div>

            <div className="mb-3">
              <label htmlFor={'source'} className="form-label"><strong>Source</strong></label>
              <select className="form-select" name="source" id="source" value={source} onChange={handleSourceChange}>
                <option key="1" value="FILE_S3">mxc raw files - S3</option>
                <option key="2" value="FILE_MANUAL">File (Manual Upload) - File</option>
                <option key="3" value="FILE_SFTP">SFTP file example -SFTP</option>
                <option key="4" value="FILE_HDFS">HDFS file example - HDFS</option>
                <option key="5" value="MYSQL">MXC Main - MYSQL</option>
                <option key="6" value="MYSQL2">CPX - MYSQL</option>
                <option key="7" value="SNOWFLAKE">MXC statement - SNOWFLAKE</option>
                {/* <option key="4" value="ELASTICSEARCH">ELASTICSEARCH US-EAST-1 - ElasticSearch</option> */}
                <option key="8" value="MSSQL">MXM - MSSQL</option>
              </select>
            </div>

            {(source === 'MYSQL' || source === 'MYSQL2' || source === 'MSSQL') && <MySQLForm />}
            {source === 'SNOWFLAKE' && <SnowflakeForm />}
            {(source === 'FILE_S3') && <FileForm type={source} etl={etl} setEtl={setEtl} />}
            {source === 'FILE_MANUAL' && (
              <div className="mb-3">
                <label htmlFor={'file'} className="form-label">File</label>
                <input type="file" className="form-control" name="file" id="file" />
              </div>
            )}
          </form>
          <button type="button" className="btn btn-success" onClick={() => nextStep()}>Next</button>
        </div>
        {/* <div className="col-4">
          <pre>{JSON.stringify(etl, null, 2)}</pre>
        </div> */}
      </div>

    </>
  );
};

const StageForm = ({ index, etl, setEtl, deleteStage }) => {
  const [fileFormat, setFileFormat] = useState('');
  const stage = etl.stages.find(stage => stage.index === index);
  const [type, setType] = useState(stage.type || 'PARSER');
  const [viewTesting, setViewTesting] = useState(false);
  const [showSpinner, setShowSpinner] = useState(false);

  useEffect(() => {
    if (viewTesting) {
      setShowSpinner(true);
      const timer = setTimeout(() => {
        setShowSpinner(false);
      }, 2000);

      return () => clearTimeout(timer);

    }
  }, [viewTesting]);

  const handleTypeChange = (e) => {
    setType(e.target.value);
    setViewTesting(false);
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, type: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleFileFormatChange = (e) => {
    setFileFormat(e.target.value);
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, fileFormat: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleChangeViewName = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, viewName: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleRecordDelimiterChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, recordDelimiter: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleFieldDelimiterChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, fieldDelimiter: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleParseHeaderChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, parseHeader: e.target.checked };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleOpEnclosedByChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, fieldOptionallyEnclosedBy: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleScapeChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, scape: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleEncodingChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, encoding: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleReplaceInvalidCharsChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, replaceInvalidHeader: e.target.checked };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleTrimSpaceChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, trimSpace: e.target.checked };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleTrimSlashChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, trimSlash: e.target.checked };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleRecordRegexChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, recordRegex: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleFieldRegexChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, fieldRegex: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleGroupTypeChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, groupType: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleGroupStartChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, groupStart: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleGroupEndChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, groupEnd: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleGroupRegexChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, groupRegex: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleGroupViewNameChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, groupViewName: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleGroupEnableIDChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, groupEnabledId: e.target.checked };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleRegexFileChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, regexFile: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleRegexEachFileChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, regexEachFile: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleIsOutputChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, isOutput: e.target.checked };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleOutputNameChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, outputName: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleAddUarChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, addUar: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleMappingChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, mapping: e.target.value };
      }
      return stage;
    }
    );
    setEtl({ ...etl, stages: newStages });
  }

  const handleMappingInputViewNameChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, mappingInputViewName: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleStageNameChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, name: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const handleSchemaChange = (e) => {
    const newStages = etl.stages.map(stage => {
      if (stage.index === index) {
        return { ...stage, schema: e.target.value };
      }
      return stage;
    });
    setEtl({ ...etl, stages: newStages });
  }

  const testStage = () => {
    setViewTesting(!viewTesting);
  }

  return (
    <div className='row'>
      <div className='col-8 mb-3'>
        <fieldset className="form-group border p-3">
          <div className="mb-3">
            <div className='mb-3' style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div style={{ marginLeft: 'auto' }}>
                <button type="button" className="btn btn-danger btn-sm" onClick={deleteStage}>Delete Stage</button>
                <button type="button" className="btn btn-primary btn-sm ms-2" onClick={testStage}>Preview</button>
              </div>
            </div>
            <div className="mb-3">
              <label htmlFor={'name'} className="form-label">Name </label>
              <input type="text" className="form-control" name="name" id="name" value={stage.name} onChange={handleStageNameChange} />
            </div>
            <label htmlFor={'type'} className="form-label">Type </label>
            <select className="form-select" name="type" id="type" value={stage.type} onChange={handleTypeChange}>
              <option key="1" value="PARSER">Parser</option>
              <option key="2" value="GROUP">Group</option>
              <option key="3" value="REGEX_FILE">RegexFile</option>
              <option key="4" value="REGEX_EACH_LINE">RegexEachLine</option>
              <option key="5" value="SQL">SQL</option>
              <option key="6" value="SIMPLE">Simple</option>
              <option key="7" value="ADD_UAR">AddUar</option>
              <option key="8" value="MAPPING">Mapping</option>
            </select>
          </div>

          {type === 'PARSER' && (
            <>
              <div className="mb-3">
                <label htmlFor={'fileFormat'} className="form-label">File format</label>
                <select className="form-select" name="fileFormat" id="fileFormat" value={stage.fileFormat} onChange={handleFileFormatChange}>
                  <option key="1" value=""></option>
                  <option key="2" value="CSV">CSV</option>
                  <option key="3" value="JSON">JSON</option>
                  <option key="4" value="BAI">BAI</option>
                  <option key="5" value="TEXT">TEXT</option>
                </select>
              </div>
              {stage.fileFormat === 'CSV' && (
                <>
                  <div className="mb-3">
                    <label htmlFor={'recordDelimiter'} className="form-label">Record Delimiter</label>
                    <input type="text" className="form-control" name="recordDelimiter" id="recordDelimiter" value={stage.recordDelimiter} onChange={handleRecordDelimiterChange} />
                  </div>
                  <div className="mb-3">
                    <label htmlFor={'fieldDelimiter'} className="form-label">Field Delimiter</label>
                    <input type="text" className="form-control" name="fieldDelimiter" id="fieldDelimiter" value={stage.fieldDelimiter} onChange={handleFieldDelimiterChange} />
                  </div>
                  <div className="form-check mb-3">
                    <input className="form-check-input" type="checkbox" value={stage.parseHeader} id="flexCheckChecked" checked={stage.parseHeader} onChange={handleParseHeaderChange} />
                    <label className="form-check-label">
                      Parse Header
                    </label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor={'fieldOptionallyEnclosedBy'} className="form-label">Field Optionally Enclosed by</label>
                    <input type="text" className="form-control" name="fieldOptionallyEnclosedBy" id="fieldOptionallyEnclosedBy" value={stage.fieldOptionallyEnclosedBy} onChange={handleOpEnclosedByChange} />
                  </div>
                  <div className="mb-3">
                    <label htmlFor={'scape'} className="form-label">Escape</label>
                    <input type="text" className="form-control" name="scape" id="scape" value={stage.scape} onChange={handleScapeChange} />
                  </div>
                  <div className="mb-3">
                    <label htmlFor={'encoding'} className="form-label">Encoding</label>
                    <input type="text" className="form-control" name="encoding" id="encoding" value={stage.encoding} onChange={handleEncodingChange} />
                  </div>
                  <div className="form-check mb-3">
                    <input className="form-check-input" type="checkbox" value={stage.replaceInvalidHeader} id="flexCheckChecked" checked={stage.replaceInvalidHeader} onChange={handleReplaceInvalidCharsChange} />
                    <label className="form-check-label">
                      Replace Invalid Characters
                    </label>
                  </div>
                  <div className="form-check mb-3">
                    <input className="form-check-input" type="checkbox" value={stage.trimSpace} id="flexCheckChecked" checked={stage.trimSpace} onChange={handleTrimSpaceChange} />
                    <label className="form-check-label">
                      Trim Space
                    </label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor={'schema'} className="form-label">Schema</label>
                    <textarea className="form-control" id="floatingTextarea" value={stage.schema} onChange={handleSchemaChange}></textarea>
                  </div>
                </>
              )}
              {stage.fileFormat === 'JSON' && (
                <>
                  <div className="mb-3">
                    <label htmlFor={'encoding'} className="form-label">Encoding</label>
                    <input type="text" className="form-control" name="encoding" id="encoding" value={stage.encoding} onChange={handleEncodingChange} />
                  </div>
                  <div className="mb-3">
                    <label htmlFor={'schema'} className="form-label">Schema</label>
                    <textarea className="form-control" id="floatingTextarea" value={stage.schema} onChange={handleSchemaChange}></textarea>
                  </div>
                </>
              )}
              {stage.fileFormat === 'BAI' && (
                <>
                  <div className="form-check mb-3">
                    <input className="form-check-input" type="checkbox" value={stage.trimSlash} id="flexCheckChecked" checked={stage.trimSlash} onChange={handleTrimSlashChange} />
                    <label className="form-check-label">
                      Trim Slash
                    </label>
                  </div>
                </>
              )}
              {stage.fileFormat === 'TEXT' && (
                <>
                  <div className="mb-3">
                    <label htmlFor={'recordRegex'} className="form-label">RecordRegex</label>
                    <input type="text" className="form-control" name="recordRegex" id="recordRegex" value={stage.recordRegex} onChange={handleRecordRegexChange} />
                  </div>
                  <div className="mb-3">
                    <label htmlFor={'fieldRegex'} className="form-label">FieldRegex</label>
                    <input type="text" className="form-control" name="fieldRegex" id="fieldRegex" value={stage.fieldRegex} onChange={handleFieldRegexChange} />
                  </div>
                  <div className="mb-3">
                    <label htmlFor={'schema'} className="form-label">Schema</label>
                    <textarea className="form-control" id="floatingTextarea" value={stage.schema} onChange={handleSchemaChange}></textarea>
                  </div>
                </>
              )}
              <div className="mb-3">
                <label htmlFor={'viewName'} className="form-label">View name</label>
                <input type="text" className="form-control" name="viewName" id="viewName" value={stage.viewName} onChange={handleChangeViewName} />
              </div>
            </>
          )}

          {type === 'GROUP' && (
            <>
              <div className="mb-3">
                <label htmlFor={'groupType'} className="form-label">Group Type</label>
                <select className="form-select" name="groupType" id="groupType" value={stage.groupType} onChange={handleGroupTypeChange}>
                  <option key="1" value=""></option>
                  <option key="2" value="SUBSTR">Substr</option>
                  <option key="3" value="REGEX">Regex</option>
                </select>
              </div>
              {stage.groupType === 'SUBSTR' && (
                <>
                  <div className="mb-3">
                    <label htmlFor={'start'} className="form-label">Start</label>
                    <input type="text" className="form-control" name="start" id="start" value={stage.groupStart} onChange={handleGroupStartChange} />
                  </div>
                  <div className="mb-3">
                    <label htmlFor={'end'} className="form-label">End</label>
                    <input type="text" className="form-control" name="end" id="end" value={stage.groupEnd} onChange={handleGroupEndChange} />
                  </div>
                </>
              )}
              {stage.groupType === 'REGEX' && (
                <>
                  <div className="mb-3">
                    <label htmlFor={'regex'} className="form-label">Regex</label>
                    <input type="text" className="form-control" name="regex" id="regex" value={stage.groupRegex} onChange={handleGroupRegexChange} />
                  </div>
                </>
              )}
              <div className="mb-3">
                <label htmlFor={'groupViewName'} className="form-label">View name</label>
                <input type="text" className="form-control" name="groupViewName" id="groupViewName" value={stage.groupViewName} onChange={handleGroupViewNameChange} />
              </div>
              <div className="form-check mb-3">
                <input className="form-check-input" type="checkbox" value={stage.groupEnabledId} id="flexCheckChecked" checked={stage.groupEnabledId} onChange={handleGroupEnableIDChange} />
                <label className="form-check-label">
                  Enable ID
                </label>
              </div>
            </>
          )}

          {type === 'REGEX_FILE' && (
            <>
              {/* <div className="mb-3">
              <label htmlFor={'viewName'} className="form-label">View name</label>
              <input type="text" className="form-control" name="viewName" id="viewName" value={stage.viewName} onChange={handleChangeViewName} />
            </div> */}
              <div className="mb-3">
                <label htmlFor={'regexFile'} className="form-label">Regex</label>
                <input type="text" className="form-control" name="regexFile" id="regexFile" value={stage.regexFile} onChange={handleRegexFileChange} />
              </div>
            </>
          )}

          {type === 'REGEX_EACH_LINE' && (
            <>
              {/* <div className="mb-3">
              <label htmlFor={'viewName'} className="form-label">View name</label>
              <input type="text" className="form-control" name="viewName" id="viewName" value={stage.viewName} onChange={handleChangeViewName} />
            </div> */}
              <div className="mb-3">
                <label htmlFor={'regexEachFile'} className="form-label">Regex</label>
                <input type="text" className="form-control" name="regexEachFile" id="regexEachFile" value={stage.regexEachFile} onChange={handleRegexEachFileChange} />
              </div>
            </>
          )}

          {type === 'SQL' && (
            <>
              <div className="mb-3">
                <label htmlFor={'sql'} className="form-label">SQL</label>
                <CodeEditor index={index} etl={etl} setEtl={setEtl} />
              </div>
              <div className="form-check mb-3">
                <input className="form-check-input" type="checkbox" value={stage.isOutput} id="flexCheckChecked" checked={stage.isOutput} onChange={handleIsOutputChange} />
                <label className="form-check-label">
                  Is Output?
                </label>
              </div>
              {stage.isOutput && (
                <div className="mb-3">
                  <label htmlFor={'outputViewName'} className="form-label">Output </label>
                  <input type="text" className="form-control" name="outputViewName" id="outputViewName" value={stage.outputName} onChange={handleOutputNameChange} />
                </div>
              )}
            </>
          )}
          {type === 'SIMPLE' && (
            <>
              <div className="mb-3">
                <label htmlFor={'sql'} className="form-label">Code</label>
                <CodeEditor index={index} etl={etl} setEtl={setEtl} />
              </div>
              <div className="form-check mb-3">
                <input className="form-check-input" type="checkbox" value={stage.isOutput} id="flexCheckChecked" checked={stage.isOutput} onChange={handleIsOutputChange} />
                <label className="form-check-label">
                  Is Output?
                </label>
              </div>
              {stage.isOutput && (
                <div className="mb-3">
                  <label htmlFor={'outputViewName'} className="form-label">Output </label>
                  <input type="text" className="form-control" name="outputViewName" id="outputViewName" value={stage.outputName} onChange={handleOutputNameChange} />
                </div>
              )}
            </>
          )}
          {type === 'MAPPING' && (
            <>
              <div className="mb-3">
                <label htmlFor={'sql'} className="form-label">Mapping</label>
                <textarea className="form-control" id="floatingTextarea" value={stage.mapping} onChange={handleMappingChange}></textarea>
              </div>
              <div className="mb-3">
                  <label htmlFor={'mappingInputViewName'} className="form-label">Input view name </label>
                  <input type="text" className="form-control" name="mappingInputViewName" id="mappingInputViewName" value={stage.mappingInputViewName} onChange={handleMappingInputViewNameChange} />
                </div>
                <div className="form-check mb-3">
                <input className="form-check-input" type="checkbox" value={stage.isOutput} id="flexCheckChecked" checked={stage.isOutput} onChange={handleIsOutputChange} />
                <label className="form-check-label">
                  Is Output?
                </label>
              </div>
              {stage.isOutput && (
                <div className="mb-3">
                  <label htmlFor={'outputViewName'} className="form-label">Output view name </label>
                  <input type="text" className="form-control" name="outputViewName" id="outputViewName" value={stage.outputName} onChange={handleOutputNameChange} />
                </div>
              )}
            </>
          )}
          {type === 'ADD_UAR' && (
            <div className="mb-3">
              <label htmlFor={'addUar'} className="form-label">Output </label>
              <input type="text" className="form-control" name="addUar" id="addUar" value={stage.addUar} onChange={handleAddUarChange} />
            </div>
          )}

          {viewTesting && showSpinner && (
            <div class="d-flex justify-content-center">
              <div class="spinner-border" role="status">
                <span class="visually-hidden">Loading...</span>
              </div>
            </div>
          )}

          {viewTesting && !showSpinner && (
            <div>
              <PreviewExample stage={stage} />
            </div>
          )}

        </fieldset>
      </div>
    </div>

  );
}

const Step2 = ({ etl, setEtl }) => {
  const { handleStep, previousStep, nextStep } = useWizard();

  const defaultTrigger = (etl.sourceType === 'FILE_S3' || etl.sourceType === 'FILE_HDFS') ? 'FILE_UPLOADED' : 'MANUAL';
  const [sourceType, setSourceType] = useState('GIT');
  const [trigger, setTrigger] = useState(defaultTrigger);
  const [cronTime, setCrontime] = useState();

  useEffect(() => {
    if (!etl.trigger) {
      setEtl({ ...etl, trigger: defaultTrigger });
    }
  }, []);

  const handleTriggerChange = (e) => {
    setTrigger(e.target.value);
    setEtl({ ...etl, trigger: e.target.value });
  }

  const handleEngineChange = (e) => {
    setTrigger(e.target.value);
    setEtl({ ...etl, engine: e.target.value });
  }

  const addNewSection = () => {
    setEtl({ ...etl, stages: [...etl.stages, { index: etl.stages[etl.stages.length - 1]?.index + 1 || 0, type: 'PARSER' }] });
  };

  const handleCronTimeChange = (e) => {
    setCrontime(e.target.value);
    setEtl({ ...etl, cronTime: e.target.value });
  }

  const deleteStage = (index) => {
    const newStages = etl.stages.filter(stage => stage.index !== index);
    setEtl({ ...etl, stages: newStages });
  }

  return (
    <>
      <div className="row">
        <div className="col-12">
          <div className='col-8'>
            <div className="mb-3">
              <label htmlFor={'trigger'} className="form-label">Trigger</label>
              <select className="form-select" name="trigger" id="trigger" value={etl.trigger} onChange={handleTriggerChange}>
                <option key="1" value="MANUAL">Manual</option>
                <option key="2" value="CRONTIME">Specific time</option>
                <option key="3" value="CRONJOB">Cron Expression</option>
                {(etl.sourceType === 'MYSQL' || etl.sourceType === 'MSSQL' || etl.sourceType === 'MYSQL2') && <option key="4" value="STREAM">Stream</option>}
                {(etl.sourceType === 'FILE_S3' || etl.sourceType === 'FILE_HDFS') && <option key="4" value="FILE_UPLOADED">When File Uploaded</option>}
              </select>
              {
                (etl.sourceType === 'FILE_S3' || etl.sourceType === 'FILE_HDFS') &&
                <>
                  <label htmlFor={'engine'} className="form-label">Engine</label>
                  <select className="form-select" name="engine" id="engine" value={etl.engine} onChange={handleEngineChange}>
                    <option key="1" value="SPARK">Spark</option>
                    <option key="2" value="SNOWFLAKE">Snowflake</option>
                  </select>
                </>
              }
            </div>
            {(trigger === 'CRONTIME' && (
              <div className="mb-3">
                <label htmlFor={'cronTime'} className="form-label">Datetime</label>
                <input type="date" className="form-control" name="cronTime" id="cronTime" value={etl.cronTime} onChange={handleCronTimeChange} />
              </div>))
              || (trigger === 'CRONJOB' && (
                <div className="mb-3">
                  <input type="text" className="form-control" name="cronExpression" id="cronExpression" placeholder="0 8 * * *" />
                </div>))
            }
            <div className="mb-3">
              {/* <label htmlFor={'file'} className="form-label">File to preview</label>
              <input type="file" className="form-control" name="file" id="file" /> */}
              <button type="button" className="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal">
                File to preview
              </button>
              
              <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-xl">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h1 className="modal-title fs-5" id="exampleModalLabel">Files in Bucket</h1>
                      <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                      <FileBrowser />
                    </div>
                    <div className="modal-footer">
                      <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Sections */}

          <h5>Stages</h5>
          {etl.stages.map((stage) => (
            <StageForm key={stage.index} index={stage.index} etl={etl} setEtl={setEtl} deleteStage={() => deleteStage(stage.index)} />
          ))}

          <div class="col-8 d-grid gap-2 mt-3">
            <button class="btn btn-success" type="button" onClick={addNewSection}>Add Stage</button>
          </div>


          <div className='mt-2'>
            <button type="button" className="btn btn-secondary" onClick={() => previousStep()}>Previous</button>
            <button type="button" className="btn btn-success ms-2" onClick={() => nextStep()}>Next</button>
          </div>

        </div>
        {/* <div className="col-4">
          <pre>{JSON.stringify(etl, null, 2)}</pre>
        </div> */}
      </div>


    </>
  );
};

const DestinationForm = ({ stage, etl, setEtl }) => {

  // useEffect(() => {
  //   console.log('Destination for', stage.index)
  //   setEtl({ ...etl, destinations: [...etl.destinations, { index: stage.index }]});
  // }, []);

  const destination = etl.destinations.find(destination => destination.index === stage.index);

  const handleDestinationChange = (e) => {
    // setDestination(e.target.value);
    const newDestinations = etl.destinations.map(destination => {
      if (destination.index === stage.index) {
        return { ...destination, type: e.target.value };
      }
      return destination;
    });
    setEtl({ ...etl, destinations: newDestinations });
  }

  return (
    <>
      <fieldset className="form-group border p-3 mb-3">
        <h6>Destination for stage: {stage.name}</h6>
        <p><strong>OutputName</strong>: {stage.outputName}</p>
        <div className="mb-3">
          <label htmlFor={'destination'} className="form-label">Destination</label>
          <select className="form-select" name="destination" id="destination" value={destination?.type} onChange={handleDestinationChange}>
            <option key="1" value=""></option>
            <option key="2" value="MYSQL">Cosmos MySQL DB - MYSQL</option>
            <option key="3" value="MYSQL2">PRTH MySQL - MYSQL</option>
            {/* <option key="4" value="SNOWFLAKE">SNOWFLAKE PRTH (PRD) - SNOWFLAKE</option>
            <option key="5" value="FILE_S3">S3 example dest - File</option> */}
            <option key="6" value="KAFKA">Kafka streaming test - KAKFA</option>
          </select>
        </div>
        {(destination?.type === 'MYSQL' || destination?.type === 'MYSQL2' || destination?.type === 'MSSQL') && <MySQLForm isDestination={true} />}
        {destination?.type === 'SNOWFLAKE' && <SnowflakeForm />}
        {destination?.type === 'FILE_S3' && <FileForm type={destination?.type} etl={etl} setEtl={setEtl} forStageIndex={stage.index} />}
        {destination?.type === 'KAFKA' && <KafkaForm etl={etl} setEtl={setEtl} forStageIndex={stage.index} />}
      </fieldset>
    </>)
};


const Step3 = ({ etl, setEtl }) => {
  const { handleStep, previousStep, nextStep } = useWizard();

  const [destination, setDestination] = useState('MYSQL');
  const handleDestinationChange = (e) => {
    setDestination(e.target.value);
  }

  const destinations = etl.stages.filter(stage => stage.isOutput);

  useEffect(() => {
    setEtl({ ...etl, destinations: destinations.map(destination => ({ index: destination.index })) });
  }, []);

  return (
    <>
      <div className="row">
        <div className="col-8">
          <form onSubmit={() => { }} noValidate>

            {destinations.map((stage, index) => (
              <DestinationForm key={index} stage={stage} etl={etl} setEtl={setEtl} />
            ))}
          </form>

          <button type="button" className="btn btn-secondary" onClick={() => previousStep()}>Previous</button>
          <button type="button" className="btn btn-success ms-2" onClick={() => nextStep()}>Submit</button>
        </div>
        {/* <div className="col-4">
          <pre>{JSON.stringify(etl, null, 2)}</pre>
        </div> */}
      </div>

    </>
  );
};
