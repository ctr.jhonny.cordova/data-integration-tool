
export default function Page() {
  return (
    <>
    <table class="table table-bordered table-hover table-condensed">
      <thead><tr><th title="Field #1">group_id03</th>
    <th title="Field #2">group_id01</th>
    <th title="Field #3">group_id02</th>
    <th title="Field #4">customerAccountNumber</th>
    <th title="Field #5">currencyCode</th>
    <th title="Field #6">openingLedgerAmount</th>
    <th title="Field #7">openingLedgerCount</th>
    <th title="Field #8">openingLedgerFundsType</th>
    <th title="Field #9">closingLedgerAmount</th>
    <th title="Field #10">closingLedgerCount</th>
    <th title="Field #11">closingLedgerFundsType</th>
    <th title="Field #12">averageClosingLedgerMTDAmount</th>
    <th title="Field #13">averageClosingLedgerMTDCount</th>
    <th title="Field #14">averageClosingLedgerMTDFundsType</th>
    <th title="Field #15">averageClosingLedgerYTDAmount</th>
    <th title="Field #16">averageClosingLedgerYTDCount</th>
    <th title="Field #17">averageClosingLedgerYTDFundsType</th>
    <th title="Field #18">collectedPlus1DayAmount</th>
    <th title="Field #19">collectedPlus1DayCount</th>
    <th title="Field #20">collectedPlus1DayFundsType</th>
    <th title="Field #21">openingCollectedAmount</th>
    <th title="Field #22">openingCollectedCount</th>
    <th title="Field #23">openingCollectedFundsType</th>
    <th title="Field #24">averageClosingAvailableMTDAmount</th>
    <th title="Field #25">averageClosingAvailableMTDCount</th>
    <th title="Field #26">averageClosingAvailableMTDFundsType</th>
    <th title="Field #27">averageClosingAvailableYTDAmount</th>
    <th title="Field #28">averageClosingAvailableYTDCount</th>
    <th title="Field #29">averageClosingAvailableYTDFundsType</th>
    <th title="Field #30">oneDayFloatAmount</th>
    <th title="Field #31">oneDayFloatCount</th>
    <th title="Field #32">oneDayFloatFundsType</th>
    <th title="Field #33">twoDayFloatAmount</th>
    <th title="Field #34">twoDayFloatCount</th>
    <th title="Field #35">twoDayFloatFundsType</th>
    <th title="Field #36">threePlusDayFloatAmount</th>
    <th title="Field #37">threePlusDayFloatCount</th>
    <th title="Field #38">threePlusDayFloatFundsType</th>
    <th title="Field #39">totalCreditsAmount</th>
    <th title="Field #40">totalCreditsCount</th>
    <th title="Field #41">totalCreditsFundsType</th>
    <th title="Field #42">creditsRealTimePaymentAmount</th>
    <th title="Field #43">creditsRealTimePaymentCount</th>
    <th title="Field #44">creditsRealTimePaymentFundsType</th>
    <th title="Field #45">totalDebitsAmount</th>
    <th title="Field #46">totalDebitsCount</th>
    <th title="Field #47">totalDebitsFundsType</th>
    <th title="Field #48">debitsRealTimePaymentAmount</th>
    <th title="Field #49">debitsRealTimePaymentCount</th>
    <th title="Field #50">debitsRealTimePaymentFundsType</th>
    </tr></thead>
    <tbody><tr>
        <td align="right">1</td>
        <td>1</td>
        <td>1</td>
        <td align="right">******1055</td>
        <td>USD</td>
        <td align="right">3603965247</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">24783293</td>
        <td align="right">789</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">41424681</td>
        <td align="right">119</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
      </tr>
      <tr>
        <td align="right">2</td>
        <td>1</td>
        <td>1</td>
        <td align="right">******2600</td>
        <td>USD</td>
        <td align="right">4814584553</td>
        <td> </td>
        <td>Z</td>
        <td align="right">4445582541</td>
        <td> </td>
        <td>Z</td>
        <td align="right">5831376811</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3520462713</td>
        <td> </td>
        <td>Z</td>
        <td align="right">4445582541</td>
        <td> </td>
        <td>Z</td>
        <td align="right">4445582541</td>
        <td> </td>
        <td>Z</td>
        <td align="right">5831376811</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3520462713</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">5540361614</td>
        <td align="right">46</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">5909363626</td>
        <td align="right">15</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
      </tr>
      <tr>
        <td align="right">3</td>
        <td>1</td>
        <td>1</td>
        <td align="right">******2618</td>
        <td>USD</td>
        <td align="right">802591</td>
        <td> </td>
        <td>Z</td>
        <td align="right">802591</td>
        <td> </td>
        <td>Z</td>
        <td align="right">811321</td>
        <td> </td>
        <td>Z</td>
        <td align="right">2484729</td>
        <td> </td>
        <td>Z</td>
        <td align="right">802591</td>
        <td> </td>
        <td>Z</td>
        <td align="right">802591</td>
        <td> </td>
        <td>Z</td>
        <td align="right">811321</td>
        <td> </td>
        <td>Z</td>
        <td align="right">2484729</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">0</td>
        <td align="right"></td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">0</td>
        <td align="right"></td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
      </tr>
  </tbody></table>
    </> );
}
