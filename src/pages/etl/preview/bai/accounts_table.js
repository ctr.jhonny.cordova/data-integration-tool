
export default function Page() {
  return (
    <>
      <div className="table-responsive">
        <table className="table table-bordered table-hover table-condensed">
          <thead><tr><th title="Field #1">id</th>
            <th title="Field #2">group_id01</th>
            <th title="Field #3">group_id02</th>
            <th title="Field #4">c1</th>
            <th title="Field #5">c2</th>
            <th title="Field #6">c3</th>
            <th title="Field #7">c4</th>
            <th title="Field #8">c5</th>
            <th title="Field #9">c6</th>
            <th title="Field #10">c7</th>
            <th title="Field #11">c8</th>
            <th title="Field #12">c9</th>
            <th title="Field #13">c10</th>
            <th title="Field #14">c11</th>
            <th title="Field #15">c12</th>
            <th title="Field #16">c13</th>
            <th title="Field #17">c14</th>
            <th title="Field #18">c15</th>
            <th title="Field #19">c16</th>
            <th title="Field #20">c17</th>
            <th title="Field #21">c18</th>
            <th title="Field #22">c19</th>
            <th title="Field #23">c20</th>
            <th title="Field #24">c21</th>
            <th title="Field #25">c22</th>
            <th title="Field #26">c23</th>
            <th title="Field #27">c24</th>
            <th title="Field #28">c25</th>
            <th title="Field #29">c26</th>
            <th title="Field #30">c27</th>
            <th title="Field #31">c28</th>
            <th title="Field #32">c29</th>
            <th title="Field #33">c30</th>
            <th title="Field #34">c31</th>
            <th title="Field #35">c32</th>
            <th title="Field #36">c33</th>
            <th title="Field #37">c34</th>
            <th title="Field #38">c35</th>
            <th title="Field #39">c36</th>
            <th title="Field #40">c37</th>
            <th title="Field #41">c38</th>
            <th title="Field #42">c39</th>
            <th title="Field #43">c40</th>
            <th title="Field #44">c41</th>
            <th title="Field #45">c42</th>
            <th title="Field #46">c43</th>
            <th title="Field #47">c44</th>
            <th title="Field #48">c45</th>
            <th title="Field #49">c46</th>
            <th title="Field #50">c47</th>
          </tr></thead>
          <tbody><tr>
            <td align="right">1</td>
            <td>1</td>
            <td>1</td>
            <td align="right">******1055</td>
            <td>USD</td>
            <td align="right">3603965247</td>
            <td> </td>
            <td>Z</td>
            <td align="right">3587323859</td>
            <td> </td>
            <td>Z</td>
            <td align="right">3600991873</td>
            <td> </td>
            <td>Z</td>
            <td align="right">3691001194</td>
            <td> </td>
            <td>Z</td>
            <td align="right">3587323859</td>
            <td> </td>
            <td>Z</td>
            <td align="right">3587323859</td>
            <td> </td>
            <td>Z</td>
            <td align="right">3600991873</td>
            <td> </td>
            <td>Z</td>
            <td align="right">3691001194</td>
            <td> </td>
            <td>Z</td>
            <td>0</td>
            <td> </td>
            <td>Z</td>
            <td>0</td>
            <td> </td>
            <td>Z</td>
            <td>0</td>
            <td> </td>
            <td>Z</td>
            <td align="right">24783293</td>
            <td align="right">789</td>
            <td>Z</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td align="right">41424681</td>
            <td align="right">119</td>
            <td>Z</td>
            <td> </td>
            <td> </td>
            <td> </td>
          </tr>
            <tr>
              <td align="right">2</td>
              <td>1</td>
              <td>1</td>
              <td align="right">******2600</td>
              <td>USD</td>
              <td align="right">4814584553</td>
              <td> </td>
              <td>Z</td>
              <td align="right">4445582541</td>
              <td> </td>
              <td>Z</td>
              <td align="right">5831376811</td>
              <td> </td>
              <td>Z</td>
              <td align="right">3520462713</td>
              <td> </td>
              <td>Z</td>
              <td align="right">4445582541</td>
              <td> </td>
              <td>Z</td>
              <td align="right">4445582541</td>
              <td> </td>
              <td>Z</td>
              <td align="right">5831376811</td>
              <td> </td>
              <td>Z</td>
              <td align="right">3520462713</td>
              <td> </td>
              <td>Z</td>
              <td>0</td>
              <td> </td>
              <td>Z</td>
              <td>0</td>
              <td> </td>
              <td>Z</td>
              <td>0</td>
              <td> </td>
              <td>Z</td>
              <td align="right">5540361614</td>
              <td align="right">46</td>
              <td>Z</td>
              <td> </td>
              <td> </td>
              <td> </td>
              <td align="right">5909363626</td>
              <td align="right">15</td>
              <td>Z</td>
              <td> </td>
              <td> </td>
              <td> </td>
            </tr>
            <tr>
              <td align="right">3</td>
              <td>1</td>
              <td>1</td>
              <td align="right">******2618</td>
              <td>USD</td>
              <td align="right">802591</td>
              <td> </td>
              <td>Z</td>
              <td align="right">802591</td>
              <td> </td>
              <td>Z</td>
              <td align="right">811321</td>
              <td> </td>
              <td>Z</td>
              <td align="right">2484729</td>
              <td> </td>
              <td>Z</td>
              <td align="right">802591</td>
              <td> </td>
              <td>Z</td>
              <td align="right">802591</td>
              <td> </td>
              <td>Z</td>
              <td align="right">811321</td>
              <td> </td>
              <td>Z</td>
              <td align="right">2484729</td>
              <td> </td>
              <td>Z</td>
              <td>0</td>
              <td> </td>
              <td>Z</td>
              <td>0</td>
              <td> </td>
              <td>Z</td>
              <td>0</td>
              <td> </td>
              <td>Z</td>
              <td align="right">0</td>
              <td align="right"></td>
              <td>Z</td>
              <td> </td>
              <td> </td>
              <td> </td>
              <td align="right">0</td>
              <td align="right"></td>
              <td>Z</td>
              <td> </td>
              <td> </td>
              <td> </td>
            </tr>
          </tbody></table>
      </div>

    </>);
}
