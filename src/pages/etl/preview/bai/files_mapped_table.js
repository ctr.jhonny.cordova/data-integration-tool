
export default function Page() {
  return (
    <table class="table">
      <thead>
        <tr>
          <th scope="col">group_id01</th>
          <th scope="col">abaFRB</th>
          <th scope="col">receiverIdentification</th>
          <th scope="col">fileCreationDate</th>
          <th scope="col">fileCreationTime</th>
          <th scope="col">fileIdentificationNumber</th>
          <th scope="col">physicalRecordLength</th>
          <th scope="col">blockSize</th>
          <th scope="col">versionNumber</th>
        </tr>
      </thead>
      <tbody class="table-group-divider">
        <tr>
          <th scope="row">1</th>
          <td>61100606</td>
          <td>61100606</td>
          <td>2024-04-05</td>
          <td>301</td>
          <td>1</td>
          <td>80</td>
          <td>1</td>
          <td>2</td>
        </tr>
      </tbody>
    </table>
  );
}
