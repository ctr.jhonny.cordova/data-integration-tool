
export default function Page() {
  return (
    <table class="table table-bordered table-hover table-condensed">
    <thead><tr><th title="Field #1">abaFRB</th>
    <th title="Field #2">receiverIdentification</th>
    <th title="Field #3">fileCreationDate</th>
    <th title="Field #4">fileCreationTime</th>
    <th title="Field #5">fileIdentificationNumber</th>
    <th title="Field #6">physicalRecordLength</th>
    <th title="Field #7">blockSize</th>
    <th title="Field #8">versionNumber</th>
    <th title="Field #9">groupStatus</th>
    <th title="Field #10">asOfDate</th>
    <th title="Field #11">asOfTime</th>
    <th title="Field #12">asOfDateModifier</th>
    <th title="Field #13">customerAccountNumber</th>
    <th title="Field #14">currencyCode</th>
    <th title="Field #15">openingLedgerAmount</th>
    <th title="Field #16">openingLedgerCount</th>
    <th title="Field #17">openingLedgerFundsType</th>
    <th title="Field #18">closingLedgerAmount</th>
    <th title="Field #19">closingLedgerCount</th>
    <th title="Field #20">closingLedgerFundsType</th>
    <th title="Field #21">averageClosingLedgerMTDAmount</th>
    <th title="Field #22">averageClosingLedgerMTDCount</th>
    <th title="Field #23">averageClosingLedgerMTDFundsType</th>
    <th title="Field #24">averageClosingLedgerYTDAmount</th>
    <th title="Field #25">averageClosingLedgerYTDCount</th>
    <th title="Field #26">averageClosingLedgerYTDFundsType</th>
    <th title="Field #27">collectedPlus1DayAmount</th>
    <th title="Field #28">collectedPlus1DayCount</th>
    <th title="Field #29">collectedPlus1DayFundsType</th>
    <th title="Field #30">openingCollectedAmount</th>
    <th title="Field #31">openingCollectedCount</th>
    <th title="Field #32">openingCollectedFundsType</th>
    <th title="Field #33">averageClosingAvailableMTDAmount</th>
    <th title="Field #34">averageClosingAvailableMTDCount</th>
    <th title="Field #35">averageClosingAvailableMTDFundsType</th>
    <th title="Field #36">averageClosingAvailableYTDAmount</th>
    <th title="Field #37">averageClosingAvailableYTDCount</th>
    <th title="Field #38">averageClosingAvailableYTDFundsType</th>
    <th title="Field #39">oneDayFloatAmount</th>
    <th title="Field #40">oneDayFloatCount</th>
    <th title="Field #41">oneDayFloatFundsType</th>
    <th title="Field #42">twoDayFloatAmount</th>
    <th title="Field #43">twoDayFloatCount</th>
    <th title="Field #44">twoDayFloatFundsType</th>
    <th title="Field #45">threePlusDayFloatAmount</th>
    <th title="Field #46">threePlusDayFloatCount</th>
    <th title="Field #47">threePlusDayFloatFundsType</th>
    <th title="Field #48">totalCreditsAmount</th>
    <th title="Field #49">totalCreditsCount</th>
    <th title="Field #50">totalCreditsFundsType</th>
    <th title="Field #51">creditsRealTimePaymentAmount</th>
    <th title="Field #52">creditsRealTimePaymentCount</th>
    <th title="Field #53">creditsRealTimePaymentFundsType</th>
    <th title="Field #54">totalDebitsAmount</th>
    <th title="Field #55">totalDebitsCount</th>
    <th title="Field #56">totalDebitsFundsType</th>
    <th title="Field #57">debitsRealTimePaymentAmount</th>
    <th title="Field #58">debitsRealTimePaymentCount</th>
    <th title="Field #59">debitsRealTimePaymentFundsType</th>
    <th title="Field #60">line_number</th>
    <th title="Field #61">group_id04</th>
    <th title="Field #62">transactionTypeCode</th>
    <th title="Field #63">transactionAmount</th>
    <th title="Field #64">transactionFundsType</th>
    <th title="Field #65">bankReferenceNumber</th>
    <th title="Field #66">customerReferenceNumber</th>
    <th title="Field #67">transactionDetailType</th>
    <th title="Field #68">transactionDetailDescription</th>
    <th title="Field #69">transactionDetailDate</th>
    <th title="Field #70">transactionDetailAccount</th>
    <th title="Field #71">transactionDetailNumber</th>
    </tr></thead>
    <tbody><tr>
        <td align="right">061100606</td>
        <td align="right">061100606</td>
        <td>2024-04-05</td>
        <td align="right">0301</td>
        <td align="right">0001</td>
        <td align="right">80</td>
        <td>1</td>
        <td align="right">2</td>
        <td>1</td>
        <td>2024-04-04</td>
        <td align="right">0000</td>
        <td align="right">2</td>
        <td align="right">******1055</td>
        <td>USD</td>
        <td align="right">3603965247</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">24783293</td>
        <td align="right">789</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">41424681</td>
        <td align="right">119</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">4</td>
        <td align="right">1</td>
        <td align="right">165</td>
        <td align="right">10.0</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739299330307108739299330307108</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739299330307108739299330307108</td>
        <td> </td>
      </tr>
      <tr>
        <td align="right">061100606</td>
        <td align="right">061100606</td>
        <td>2024-04-05</td>
        <td align="right">0301</td>
        <td align="right">0001</td>
        <td align="right">80</td>
        <td>1</td>
        <td align="right">2</td>
        <td>1</td>
        <td>2024-04-04</td>
        <td align="right">0000</td>
        <td align="right">2</td>
        <td align="right">******1055</td>
        <td>USD</td>
        <td align="right">3603965247</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">24783293</td>
        <td align="right">789</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">41424681</td>
        <td align="right">119</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">5</td>
        <td align="right">2</td>
        <td align="right">165</td>
        <td align="right">100.0</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739297085300708739297085300708</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739297085300708739297085300708</td>
        <td> </td>
      </tr>
      <tr>
        <td align="right">061100606</td>
        <td align="right">061100606</td>
        <td>2024-04-05</td>
        <td align="right">0301</td>
        <td align="right">0001</td>
        <td align="right">80</td>
        <td>1</td>
        <td align="right">2</td>
        <td>1</td>
        <td>2024-04-04</td>
        <td align="right">0000</td>
        <td align="right">2</td>
        <td align="right">******1055</td>
        <td>USD</td>
        <td align="right">3603965247</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">24783293</td>
        <td align="right">789</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">41424681</td>
        <td align="right">119</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">6</td>
        <td align="right">3</td>
        <td align="right">165</td>
        <td align="right">100.0</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739290400300401739290400300401</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739290400300401739290400300401</td>
        <td> </td>
      </tr>
      <tr>
        <td align="right">061100606</td>
        <td align="right">061100606</td>
        <td>2024-04-05</td>
        <td align="right">0301</td>
        <td align="right">0001</td>
        <td align="right">80</td>
        <td>1</td>
        <td align="right">2</td>
        <td>1</td>
        <td>2024-04-04</td>
        <td align="right">0000</td>
        <td align="right">2</td>
        <td align="right">******1055</td>
        <td>USD</td>
        <td align="right">3603965247</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">24783293</td>
        <td align="right">789</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">41424681</td>
        <td align="right">119</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">7</td>
        <td align="right">4</td>
        <td align="right">165</td>
        <td align="right">100.01</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739763938144486739763938144486</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739763938144486739763938144486</td>
        <td> </td>
      </tr>
      <tr>
        <td align="right">061100606</td>
        <td align="right">061100606</td>
        <td>2024-04-05</td>
        <td align="right">0301</td>
        <td align="right">0001</td>
        <td align="right">80</td>
        <td>1</td>
        <td align="right">2</td>
        <td>1</td>
        <td>2024-04-04</td>
        <td align="right">0000</td>
        <td align="right">2</td>
        <td align="right">******1055</td>
        <td>USD</td>
        <td align="right">3603965247</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">24783293</td>
        <td align="right">789</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">41424681</td>
        <td align="right">119</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">8</td>
        <td align="right">5</td>
        <td align="right">165</td>
        <td align="right">10.02</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739299060300376739299060300376</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739299060300376739299060300376</td>
        <td> </td>
      </tr>
      <tr>
        <td align="right">061100606</td>
        <td align="right">061100606</td>
        <td>2024-04-05</td>
        <td align="right">0301</td>
        <td align="right">0001</td>
        <td align="right">80</td>
        <td>1</td>
        <td align="right">2</td>
        <td>1</td>
        <td>2024-04-04</td>
        <td align="right">0000</td>
        <td align="right">2</td>
        <td align="right">******1055</td>
        <td>USD</td>
        <td align="right">3603965247</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">24783293</td>
        <td align="right">789</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">41424681</td>
        <td align="right">119</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">9</td>
        <td align="right">6</td>
        <td align="right">165</td>
        <td align="right">10.02</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739299060300412739299060300412</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739299060300412739299060300412</td>
        <td> </td>
      </tr>
      <tr>
        <td align="right">061100606</td>
        <td align="right">061100606</td>
        <td>2024-04-05</td>
        <td align="right">0301</td>
        <td align="right">0001</td>
        <td align="right">80</td>
        <td>1</td>
        <td align="right">2</td>
        <td>1</td>
        <td>2024-04-04</td>
        <td align="right">0000</td>
        <td align="right">2</td>
        <td align="right">******1055</td>
        <td>USD</td>
        <td align="right">3603965247</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">24783293</td>
        <td align="right">789</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">41424681</td>
        <td align="right">119</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">10</td>
        <td align="right">7</td>
        <td align="right">165</td>
        <td align="right">10.02</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739299060300422739299060300422</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739299060300422739299060300422</td>
        <td> </td>
      </tr>
      <tr>
        <td align="right">061100606</td>
        <td align="right">061100606</td>
        <td>2024-04-05</td>
        <td align="right">0301</td>
        <td align="right">0001</td>
        <td align="right">80</td>
        <td>1</td>
        <td align="right">2</td>
        <td>1</td>
        <td>2024-04-04</td>
        <td align="right">0000</td>
        <td align="right">2</td>
        <td align="right">******1055</td>
        <td>USD</td>
        <td align="right">3603965247</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3587323859</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3600991873</td>
        <td> </td>
        <td>Z</td>
        <td align="right">3691001194</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td>0</td>
        <td> </td>
        <td>Z</td>
        <td align="right">24783293</td>
        <td align="right">789</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">41424681</td>
        <td align="right">119</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td align="right">11</td>
        <td align="right">8</td>
        <td align="right">165</td>
        <td align="right">1003.15</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739294900300070739294900300070</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739294900300070739294900300070</td>
        <td> </td>
      </tr>
      <tr>
        <td align="right">061100606</td>
        <td align="right">061100606</td>
        <td>2024-04-05</td>
        <td align="right">0301</td>
        <td align="right">0001</td>
        <td align="right">80</td>
        <td>1</td>
        <td align="right">2</td>
    <td>1</td>
    <td>2024-04-04</td>
    <td align="right">0000</td>
    <td align="right">2</td>
    <td align="right">******1055</td>
    <td>USD</td>
    <td align="right">3603965247</td>
    <td> </td>
    <td>Z</td>
    <td align="right">3587323859</td>
    <td> </td>
    <td>Z</td>
    <td align="right">3600991873</td>
    <td> </td>
    <td>Z</td>
    <td align="right">3691001194</td>
    <td> </td>
    <td>Z</td>
    <td align="right">3587323859</td>
    <td> </td>
    <td>Z</td>
    <td align="right">3587323859</td>
    <td> </td>
    <td>Z</td>
    <td align="right">3600991873</td>
    <td> </td>
    <td>Z</td>
    <td align="right">3691001194</td>
    <td> </td>
    <td>Z</td>
    <td>0</td>
    <td> </td>
    <td>Z</td>
    <td>0</td>
    <td> </td>
    <td>Z</td>
    <td>0</td>
    <td> </td>
    <td>Z</td>
    <td align="right">24783293</td>
    <td align="right">789</td>
    <td>Z</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td align="right">41424681</td>
    <td align="right">119</td>
    <td>Z</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td align="right">12</td>
    <td align="right">9</td>
    <td align="right">165</td>
    <td align="right">10.07</td>
    <td>Z</td>
    <td> </td>
    <td> </td>
    <td>BANKCARD DEP MERCH DEP 240404 739297705300837739297705300837</td>
    <td>MERCH DEP</td>
    <td>240404</td>
    <td>739297705300837739297705300837</td>
    <td> </td>
  </tr>
  </tbody></table>
  );
}
