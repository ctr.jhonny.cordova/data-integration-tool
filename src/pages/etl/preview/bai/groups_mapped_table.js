
export default function Page() {
  return (
    <table class="table">
      <thead>
        <tr>
          <th scope="col">group_id02</th>
          <th scope="col">group_id01</th>
          <th scope="col">groupStatus</th>
          <th scope="col">asOfDate</th>
          <th scope="col">asOfTime</th>
          <th scope="col">asOfDateModifier</th>
        </tr>
      </thead>
      <tbody class="table-group-divider">
        <tr>
          <th scope="row">1</th>
          <td>1</td>
          <td>1</td>
          <td>2024-04-04</td>
          <td>0</td>
          <td>2</td>
        </tr>
      </tbody>
    </table>
  );
}
