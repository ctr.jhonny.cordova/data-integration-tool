import AccountsTable from './accounts_table';
import TransactionsTable from './transactions_table';

export default function Page() {
  return (<div>

    <p><strong>BankActivities_01</strong></p>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">id</th>
          <th scope="col">c1</th>
          <th scope="col">c2</th>
          <th scope="col">c3</th>
          <th scope="col">c4</th>
          <th scope="col">c5</th>
          <th scope="col">c6</th>
          <th scope="col">c7</th>
          <th scope="col">c8</th>
        </tr>
      </thead>
      <tbody class="table-group-divider">
        <tr>
          <th scope="row">1</th>
          <td>61100606</td>
          <td>61100606</td>
          <td>2024-04-05</td>
          <td>301</td>
          <td>1</td>
          <td>80</td>
          <td>1</td>
          <td>2</td>
        </tr>
      </tbody>
    </table>

    <p><strong>BankActivities_02</strong></p>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">id</th>
          <th scope="col">group_id01</th>
          <th scope="col">c1</th>
          <th scope="col">c2</th>
          <th scope="col">c3</th>
          <th scope="col">c4</th>
        </tr>
      </thead>
      <tbody class="table-group-divider">
        <tr>
          <th scope="row">1</th>
          <td>1</td>
          <td>1</td>
          <td>2024-04-04</td>
          <td>0</td>
          <td>2</td>
        </tr>
      </tbody>
    </table>

    <p><strong>BankActivities_03</strong></p>
    <AccountsTable />

    <p><strong>BankActivities_16</strong></p>
    <TransactionsTable/>
  </div>)
}
