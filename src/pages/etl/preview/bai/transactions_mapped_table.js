
export default function Page() {
  return (
    <>
    <table class="table table-bordered table-hover table-condensed">
    <thead><tr><th title="Field #1">id</th>
    <th title="Field #2">group_id01</th>
    <th title="Field #3">group_id02</th>
    <th title="Field #4">group_id03</th>
    <th title="Field #5">transactionTypeCode</th>
    <th title="Field #6">transactionAmount</th>
    <th title="Field #7">transactionFundsType</th>
    <th title="Field #8">bankReferenceNumber</th>
    <th title="Field #9">customerReferenceNumber</th>
    <th title="Field #10">transactionDetailType</th>
    <th title="Field #11">transactionDetailDescription</th>
    <th title="Field #12">transactionDetailDate</th>
    <th title="Field #13">transactionDetailAccount</th>
    <th title="Field #14">transactionDetailNumber</th>
    </tr></thead>
    <tbody><tr>
        <td align="right">1</td>
        <td>1</td>
        <td>1</td>
        <td align="right">1</td>
        <td align="right">165</td>
        <td align="right">1000</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739299330307108739299330307108</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739299330307108739299330307108</td>
      </tr>
      <tr>
        <td align="right">2</td>
        <td>1</td>
        <td>1</td>
        <td align="right">1</td>
        <td align="right">165</td>
        <td align="right">10000</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739297085300708739297085300708</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739297085300708739297085300708</td>
      </tr>
      <tr>
        <td align="right">3</td>
        <td>1</td>
        <td>1</td>
        <td align="right">1</td>
        <td align="right">165</td>
        <td align="right">10000</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739290400300401739290400300401</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739290400300401739290400300401</td>
      </tr>
      <tr>
        <td align="right">4</td>
        <td>1</td>
        <td>1</td>
        <td align="right">1</td>
        <td align="right">165</td>
        <td align="right">10001</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739763938144486739763938144486</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739763938144486739763938144486</td>
      </tr>
      <tr>
        <td align="right">5</td>
        <td>1</td>
        <td>1</td>
        <td align="right">2</td>
        <td align="right">165</td>
        <td align="right">1002</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739299060300376739299060300376</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739299060300376739299060300376</td>
      </tr>
      <tr>
        <td align="right">6</td>
        <td>1</td>
        <td>1</td>
        <td align="right">2</td>
        <td align="right">165</td>
        <td align="right">1002</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739299060300412739299060300412</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739299060300412739299060300412</td>
      </tr>
      <tr>
        <td align="right">7</td>
        <td>1</td>
        <td>1</td>
        <td align="right">2</td>
        <td align="right">165</td>
        <td align="right">1002</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739299060300422739299060300422</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739299060300422739299060300422</td>
      </tr>
      <tr>
        <td align="right">8</td>
        <td>1</td>
        <td>1</td>
        <td align="right">2</td>
        <td align="right">165</td>
        <td align="right">100315</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739294900300070739294900300070</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739294900300070739294900300070</td>
      </tr>
      <tr>
        <td align="right">9</td>
        <td>1</td>
        <td>1</td>
        <td align="right">2</td>
        <td align="right">165</td>
        <td align="right">1007</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739297705300837739297705300837</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739297705300837739297705300837</td>
      </tr>
      <tr>
        <td align="right">10</td>
        <td>1</td>
        <td>1</td>
        <td align="right">2</td>
        <td align="right">165</td>
        <td align="right">10155</td>
        <td>Z</td>
        <td> </td>
        <td> </td>
        <td>BANKCARD DEP MERCH DEP 240404 739298805300109739298805300109</td>
        <td>MERCH DEP</td>
        <td>240404</td>
        <td>739298805300109739298805300109</td>
      </tr>
  </tbody></table>
    </> );
}
