
import BaiParserTable from './bai/parser_table';
import SQLTable from './sql_table';
import MassParserTable from './mass/parser_table';

export default function Page({stage}) {
  stage = stage ?? {};


  console.log(stage);
  const type = stage?.type ?? 'None';
  const code = stage?.code ?? '';
  console.log(`${type} stage`);

  switch(stage.type) {
    case 'PARSER':
      if(stage?.fileFormat === 'BAI') return BaiParserTable(stage);
      if(stage?.fileFormat === 'CSV' && stage?.fieldDelimiter==='~') return MassParserTable(stage);
    case 'SQL':
      let r = SQLTable(stage);
      if(r) return r;
    default:
      return OutputExample();
  }
}

const MassExample = () => {
  return (<div>

    <p>Output (example)</p>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">test_columnA</th>
          <th scope="col">test_columnB</th>
        </tr>
      </thead>
      <tbody class="table-group-divider">
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Jacob</td>
          <td>Thornton</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td >Test</td>
          <td >Test</td>
        </tr>
      </tbody>
    </table>
  </div>)
}

const OutputExample = () => {
  return (<div>

    <p>Output (example)</p>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">test_columnA</th>
          <th scope="col">test_columnB</th>
        </tr>
      </thead>
      <tbody class="table-group-divider">
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Jacob</td>
          <td>Thornton</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td >Test</td>
          <td >Test</td>
        </tr>
      </tbody>
    </table>
  </div>)
}
