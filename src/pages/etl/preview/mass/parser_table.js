
export default function Page({viewName}) {
  return (<div>

    <p><strong>{viewName}</strong></p>
    <table class="table table-bordered table-hover table-condensed">
      <thead><tr><th title="Field #1">SEQUENCE NUMBER</th>
    <th title="Field #2">FDMSACCOUNTNO</th>
    <th title="Field #3">BUSINESS CHAIN NO</th>
    <th title="Field #4">BANK CHAIN NO</th>
    <th title="Field #5">AGENT CHAIN NO</th>
    <th title="Field #6">CORP CHAIN NO</th>
    <th title="Field #7">CHAIN CHAIN NO</th>
    <th title="Field #8">EXTERNAL NO</th>
    <th title="Field #9">EXTERNAL IND</th>
    <th title="Field #10">ACCOUNT STATUS</th>
    <th title="Field #11">PAY CODE</th>
    <th title="Field #12">PROCESS MODE</th>
    <th title="Field #13">RECAP CODE</th>
    <th title="Field #14">SECURITY CODE</th>
    <th title="Field #15">STORE NO</th>
    <th title="Field #16">LINK BACKWARD MERCH</th>
    <th title="Field #17">GLOBAL FEE NUMBER</th>
    <th title="Field #18">GLOBAL RATE NUMBER</th>
    <th title="Field #19">RELATIONSHIP MNGR CODE</th>
    <th title="Field #20">PAY CYCLE</th>
    <th title="Field #21">PREVPROCESSMODE</th>
    <th title="Field #22">PREVACCOUNTSTATUS</th>
    <th title="Field #23">BRANCHNUMBER</th>
    <th title="Field #24">MFPLATFORM</th>
    </tr></thead>
    <tbody><tr>
        <td>AA</td>
        <td align="right">815123480882        </td>
        <td align="right">815123480882        </td>
        <td align="right">000000000000        </td>
        <td align="right">000000000000        </td>
        <td align="right">000000000000        </td>
        <td align="right">000000000000        </td>
        <td align="right">000000000000000     </td>
        <td>N</td>
        <td align="right">16</td>
        <td align="right">21</td>
        <td align="right">9</td>
        <td align="right">07</td>
        <td align="right">    </td>
        <td>        </td>
        <td align="right">                    </td>
        <td align="right">000000000000        </td>
        <td align="right">0                   </td>
        <td>PRH</td>
        <td align="right">00</td>
        <td> </td>
        <td align="right">  </td>
        <td align="right">0000</td>
        <td>T</td>
      </tr>
      <tr>
        <td>AA</td>
        <td align="right">815200061886        </td>
        <td align="right">815123480882        </td>
        <td align="right">815986870880        </td>
        <td align="right">815976870882        </td>
        <td align="right">000000000000        </td>
        <td align="right">815200061886        </td>
        <td align="right">000000000000000     </td>
        <td>N</td>
        <td align="right">13</td>
        <td align="right">21</td>
        <td align="right">3</td>
        <td align="right">01</td>
        <td align="right">4000</td>
        <td>        </td>
        <td align="right">936000001           </td>
        <td align="right">000000000000        </td>
        <td align="right">000000000000        </td>
        <td>CSM</td>
        <td align="right">00</td>
        <td> </td>
        <td align="right">01</td>
        <td align="right">    </td>
        <td>T</td>
      </tr>
      <tr>
        <td>AA</td>
        <td align="right">815200079888        </td>
        <td align="right">815123480882        </td>
        <td align="right">815986870880        </td>
        <td align="right">815976870882        </td>
        <td align="right">000000000000        </td>
        <td align="right">815200079888        </td>
        <td align="right">000000000000000     </td>
        <td>N</td>
        <td align="right">16</td>
        <td align="right">21</td>
        <td align="right">3</td>
        <td align="right">07</td>
        <td align="right">4000</td>
        <td>        </td>
        <td align="right">815000001           </td>
        <td align="right">000000000000        </td>
        <td align="right">000000000000        </td>
        <td>PRH</td>
        <td align="right">00</td>
        <td> </td>
        <td align="right">  </td>
        <td align="right">    </td>
        <td>T</td>
      </tr>
      <tr>
        <td>AA</td>
        <td align="right">815200082882        </td>
        <td align="right">815123480882        </td>
        <td align="right">815986870880        </td>
        <td align="right">815976870882        </td>
        <td align="right">000000000000        </td>
        <td align="right">815200082882        </td>
        <td align="right">000000000000000     </td>
        <td>N</td>
        <td align="right">16</td>
        <td align="right">21</td>
        <td align="right">3</td>
        <td align="right">07</td>
        <td align="right">4000</td>
        <td>        </td>
        <td align="right">815000001           </td>
        <td align="right">000000000000        </td>
        <td align="right">000000000000        </td>
        <td>PRH</td>
        <td align="right">00</td>
        <td> </td>
        <td align="right">  </td>
        <td align="right">    </td>
        <td>T</td>
      </tr>
      <tr>
        <td>AA</td>
        <td align="right">815200083880        </td>
        <td align="right">815123480882        </td>
        <td align="right">815986870880        </td>
        <td align="right">815976870882        </td>
        <td align="right">000000000000        </td>
        <td align="right">815200083880        </td>
        <td align="right">000000000000000     </td>
        <td>N</td>
        <td align="right">16</td>
        <td align="right">21</td>
        <td align="right">3</td>
        <td align="right">07</td>
        <td align="right">4000</td>
        <td>        </td>
        <td align="right">815000001           </td>
        <td align="right">000000000000        </td>
        <td align="right">000000000000        </td>
        <td>PRH</td>
        <td align="right">00</td>
        <td> </td>
        <td align="right">  </td>
        <td align="right">    </td>
        <td>T</td>
      </tr>
      <tr>
        <td>AA</td>
        <td align="right">815200084888        </td>
        <td align="right">815123480882        </td>
        <td align="right">815986870880        </td>
        <td align="right">815976870882        </td>
        <td align="right">000000000000        </td>
        <td align="right">815200084888        </td>
        <td align="right">000000000000000     </td>
        <td>N</td>
        <td align="right">16</td>
        <td align="right">21</td>
        <td align="right">3</td>
        <td align="right">07</td>
        <td align="right">4000</td>
        <td>        </td>
        <td align="right">815000001           </td>
        <td align="right">000000000000        </td>
        <td align="right">000000000000        </td>
        <td>PRH</td>
        <td align="right">00</td>
        <td> </td>
        <td align="right">  </td>
        <td align="right">    </td>
        <td>T</td>
      </tr>
      <tr>
        <td>AA</td>
        <td align="right">815200086883        </td>
        <td align="right">815123480882        </td>
        <td align="right">815986870880        </td>
        <td align="right">815976870882        </td>
        <td align="right">000000000000        </td>
        <td align="right">815200086883        </td>
        <td align="right">000000000000000     </td>
        <td>N</td>
        <td align="right">16</td>
        <td align="right">21</td>
        <td align="right">3</td>
        <td align="right">07</td>
        <td align="right">4000</td>
        <td>        </td>
        <td align="right"> </td>
        <td align="right"> </td>
        <td align="right"> </td>
        <td> </td>
        <td align="right"> </td>
        <td> </td>
        <td align="right"> </td>
        <td align="right"> </td>
        <td> </td>
      </tr>
  </tbody></table>

  </div>)
}
