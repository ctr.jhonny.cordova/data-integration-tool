import GroupsMappedTable from './bai/groups_mapped_table';
import FilesMappedTable from './bai/files_mapped_table';
import AccountsMappedTable from './bai/accounts_mapped_table';
import TransactionsMappedTable from './bai/transactions_mapped_table';
import FinalTable from './bai/final_table';
import MassParserTable from './mass/parser_table';

export default function Page({code}) {

  if(!code) return null;
  console.log('asdasd', code);

  if(code.match(/CREATE OR REPLACE TEMPORARY VIEW df_files_mapped.*/))
    return <FilesMappedTable />;
  else if(code.match(/CREATE OR REPLACE TEMPORARY VIEW df_groups_mapped.*/))
    return <GroupsMappedTable />;
  else if(code.match(/CREATE OR REPLACE TEMPORARY VIEW df_accounts_mapped.*/))
    return <AccountsMappedTable />;
  else if(code.match(/CREATE OR REPLACE TEMPORARY VIEW df_transactions_mapped.*/))
    return <TransactionsMappedTable />;
  else if(code.match(/FROM df_files_mapped f.*/))
    return <FinalTable />;
  else if(code.match(/from mass.*/))
    return <MassParserTable viewName=''/>;


  return null;
}
